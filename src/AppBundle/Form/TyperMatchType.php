<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TyperMatchType extends AbstractType
{
    private $container;


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->setContainer($options['container']);
        $builder
            ->add('dateTime', 'datetime', array('widget' => 'single_text',
                'date_format' => 'yyyy-MM-dd  HH:mm'))
            ->add('resultA', null, array('label' => false))
            ->add('resultB', null, array('label' => false))
            ->add('teamA', EntityType::class, array(
                'class' => 'AppBundle:TyperTeam',
                'choice_label' => 'name',
                'empty_value' => "Wybierz drużynę",
                'choices_as_values' => true,
                'choice_attr' => function ($val, $key, $index) {
                    return ['data-thumbnail' => $this->getContainer()->get('liip_imagine.cache.manager')->getBrowserPath($val->getPathLogo(), 'thumb_team'),
                    ];
                },))
            ->add('teamB', EntityType::class, array(
                'class' => 'AppBundle:TyperTeam',
                'choice_label' => 'name',
                'empty_value' => "Wybierz drużynę",
                'choices_as_values' => true,
                'choice_attr' => function ($val, $key, $index) {
                    return ['data-thumbnail' => $this->getContainer()->get('liip_imagine.cache.manager')->getBrowserPath($val->getPathLogo(), 'thumb_team'),
                    ];
                },));

    }

    /**
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param mixed $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TyperMatch'
        ));
    }

    public function getParent()
    {
        return 'container_aware';
    }

}
