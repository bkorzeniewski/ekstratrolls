<?php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;


    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Główna', array('route' => 'object_index'));
        $menu->addChild('Mem Generator')->setUri('/#');
        $menu->addChild('Poczekalnia', array('route' => 'object_waiting'));
        $menu->addChild('Tabela', array('route' => 'tablefootball_index'));
        $menu->addChild('Artykuły', array('route' => 'article_index'));
        $menu->addChild('Liga typerów')->setUri('/#');



        return $menu;

    }

    public function topMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Kontakt')->setUri('/#');
        $menu->addChild('Regulamin')->setUri('/#');

        $menu->addChild('Logowanie', array('route' => 'facebook_login'));
        $menu->addChild('Rejestracja')->setUri('/#');

        return $menu;
    }

    public function profileMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Profil', array('route' => 'user_profile'));
        $menu->addChild('Ustawienia', array('route' => 'object_index'));
        $menu->addChild('Wyloguj')->setUri('/logout');;

        return $menu;
    }

    public function footerMenu(FactoryInterface $factory, array $options){
        $menu = $this->mainMenu($factory, $options);
        $menu->addChild('Kontakt')->setUri('/#');
        $menu->addChild('Regulamin')->setUri('/#');

        return $menu;
    }


}