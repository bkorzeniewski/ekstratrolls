<?php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CommentVoteValidator extends ConstraintValidator
{

    protected $manager;
    protected $token_storage;
    protected $container;

    public function __construct($token_storage, $manager, $container)
    {
        $this->token_storage = $token_storage;
        $this->manager = $manager;
        $this->container = $container;
    }

    public function validate($vote, Constraint $constraint)
    {
        $em = $this->manager;

        $existingVote = $em->getRepository('AppBundle:CommentVote')->findOneBy(array(
            'voter' => $this->token_storage->getToken()->getUser(),
            'comment' => $vote->getComment(),
            'value' => $vote->getValue()
        ));

        if ($existingVote) {
                $this->context->addViolation($constraint->message);
        }

    }

}