<?php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CommentVote extends Constraint
{
    public $message = 'Już głosowałeś';


    public function validatedBy()
    {
        return 'alias_name';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }



}