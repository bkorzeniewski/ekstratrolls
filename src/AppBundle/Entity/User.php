<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;

/**
 * User
 */
class User extends BaseUser
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $facebook_id;

    /**
     * @var string
     */
    protected $facebook_access_token;

    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var string
     */
    private $avatar;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $objects;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $votes;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $comments;

    /**
     * @var string
     */
    public $thumbAvatar;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $typerTypeMatch;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $answers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $articles;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->objects = new \Doctrine\Common\Collections\ArrayCollection();
        $this->votes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->typerTypeMatch = new \Doctrine\Common\Collections\ArrayCollection();
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
        parent::__construct();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set facebook_id
     *
     * @param string $facebookId
     * @return User
     */
    public function setFacebookId($facebookId)
    {
        $this->facebook_id = $facebookId;

        return $this;
    }

    /**
     * Get facebook_id
     *
     * @return string 
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    /**
     * Set facebook_access_token
     *
     * @param string $facebookAccessToken
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebook_access_token = $facebookAccessToken;

        return $this;
    }

    /**
     * Get facebook_access_token
     *
     * @return string 
     */
    public function getFacebookAccessToken()
    {
        return $this->facebook_access_token;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;

    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {

        return $this->avatar ? $this->avatar : 'default.png';
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return User
     */
    public function setAvatar($avatar)
    {
        if($this->getFacebookId()){

            $facebookUrlProfile = "http://graph.facebook.com/".$avatar."/picture?type=large";

            $imageName = uniqid() . '.jpg';

            $isAdded = file_put_contents($this->getUploadRootDir() . $imageName, file_get_contents($facebookUrlProfile));

            if ($isAdded) {

                $this->avatar = $imageName;
            }

        }else{

            $this->avatar = $avatar;
        }


        return $this;
    }

    public function getAbsolutePath()
    {
        return null === $this->avatar ? null : $this->getUploadRootDir() . $this->getAvatar();
    }

    public function getWebPath()
    {
        return $this->getUploadDir() . $this->getAvatar();
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../web' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return '/uploads/avatar/';
    }

    public function removeFile()
    {
        if ($file = $this->getAbsolutePath()) {
            @unlink($file);
        }
    }

    /**
     * Add objects
     *
     * @param \AppBundle\Entity\Object $objects
     * @return User
     */
    public function addObject(\AppBundle\Entity\Object $objects)
    {
        $this->objects[] = $objects;

        return $this;
    }

    /**
     * Remove objects
     *
     * @param \AppBundle\Entity\Object $objects
     */
    public function removeObject(\AppBundle\Entity\Object $objects)
    {
        $this->objects->removeElement($objects);
    }

    /**
     * Get objects
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getObjects()
    {
        return $this->objects;
    }

    /**
     * Add votes
     *
     * @param \AppBundle\Entity\ObjectVote $votes
     * @return User
     */
    public function addVote(\AppBundle\Entity\ObjectVote $votes)
    {
        $this->votes[] = $votes;

        return $this;
    }

    /**
     * Remove votes
     *
     * @param \AppBundle\Entity\ObjectVote $votes
     */
    public function removeVote(\AppBundle\Entity\ObjectVote $votes)
    {
        $this->votes->removeElement($votes);
    }

    /**
     * Get votes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * Get user object vote
     *
     * @param \AppBundle\Entity\Object $object
     *
     * @return Vote
     */

    public function getVoteObject(\AppBundle\Entity\Object $object) {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("object", $object))
        ;

        return $this->getVotes()->matching($criteria)[0];
    }
    /**
     * Add comments
     *
     * @param \AppBundle\Entity\Comment $comments
     * @return User
     */
    public function addComment(\AppBundle\Entity\Comment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \AppBundle\Entity\Comment $comments
     */
    public function removeComment(\AppBundle\Entity\Comment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTyperTypeMatch()
    {
        return $this->typerTypeMatch;
    }

    /**
     * @param \AppBundle\Entity\TyperTypeMatch $typerTypeMatch
     */
    public function addTyperTypeMatch(\AppBundle\Entity\TyperTypeMatch $typerTypeMatch)
    {
        $this->typerTypeMatch[] = $typerTypeMatch;
    }


    /**
     * Remove typerTypeMatch
     *
     * @param \AppBundle\Entity\TyperTypeMatch $typerTypeMatch
     */
    public function removeTyperTypeMatch(\AppBundle\Entity\TyperTypeMatch $typerTypeMatch)
    {
        $this->typerTypeMatch->removeElement($typerTypeMatch);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $answers
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;
    }


    /**
     * Add answers
     *
     * @param \AppBundle\Entity\Answer $answers
     * @return Club
     */
    public function addAnswer(\AppBundle\Entity\Answer $answers)
    {
        $this->answers[] = $answers;

        return $this;
    }

    /**
     * Remove answers
     *
     * @param \AppBundle\Entity\Answer $answers
     */
    public function removeAnswer(\AppBundle\Entity\Answer $answers)
    {
        $this->answers->removeElement($answers);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $articles
     */
    public function setArticles($articles)
    {
        $this->articles = $articles;
    }

    /**
     * Add articles
     *
     * @param \AppBundle\Entity\Article $articles
     * @return Club
     */
    public function addArticle(\AppBundle\Entity\Article $articles)
    {
        $this->articles[] = $articles;

        return $this;
    }

    /**
     * Remove articles
     *
     * @param \AppBundle\Entity\Article $articles
     */
    public function removeArticle(\AppBundle\Entity\Article $articles)
    {
        $this->articles->removeElement($articles);
    }

}
