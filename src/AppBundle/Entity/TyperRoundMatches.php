<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TyperRoundMatches
 */
class TyperRoundMatches
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $numberRound;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numberRound
     *
     * @param integer $numberRound
     * @return TyperRoundMatches
     */
    public function setNumberRound($numberRound)
    {
        $this->numberRound = $numberRound;

        return $this;
    }

    /**
     * Get numberRound
     *
     * @return integer 
     */
    public function getNumberRound()
    {
        return $this->numberRound;
    }
    /**
     * @var \AppBundle\Entity\TyperMatch
     */
    private $typerMatches;


    /**
     * Set typerMatches
     *
     * @param \AppBundle\Entity\TyperMatch $typerMatches
     * @return TyperRoundMatches
     */
    public function setTyperMatches(\AppBundle\Entity\TyperMatch $typerMatches = null)
    {
        $this->typerMatches = $typerMatches;

        return $this;
    }

    /**
     * Get typerMatches
     *
     * @return \AppBundle\Entity\TyperMatch 
     */
    public function getTyperMatches()
    {
        return $this->typerMatches;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->typerMatches = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add typerMatches
     *
     * @param \AppBundle\Entity\TyperMatch $typerMatches
     * @return TyperRoundMatches
     */
    public function addTyperMatch(\AppBundle\Entity\TyperMatch $typerMatches)
    {
        $this->typerMatches[] = $typerMatches;

        return $this;
    }

    /**
     * Remove typerMatches
     *
     * @param \AppBundle\Entity\TyperMatch $typerMatches
     */
    public function removeTyperMatch(\AppBundle\Entity\TyperMatch $typerMatches)
    {
        $this->typerMatches->removeElement($typerMatches);
    }
}
