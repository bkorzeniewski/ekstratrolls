<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ObjectVote
 */
class ObjectVote
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $vote;

    /**
     * @var \AppBundle\Entity\Object
     */
    private $object;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vote
     *
     * @param integer $vote
     * @return ObjectVote
     */
    public function setObjectVote($vote)
    {
        $this->vote = $vote;

        return $this;
    }

    /**
     * Get vote
     *
     * @return integer 
     */
    public function getObjectVote()
    {
        return $this->vote;
    }

    /**
     * Set object
     *
     * @param \AppBundle\Entity\Object $object
     * @return ObjectVote
     */
    public function setObject(\AppBundle\Entity\Object $object = null)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * Get object
     *
     * @return \AppBundle\Entity\Object 
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return ObjectVote
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
