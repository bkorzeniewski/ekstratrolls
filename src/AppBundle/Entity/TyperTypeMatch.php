<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TyperTypeMatch
 */
class TyperTypeMatch
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $resultA;

    /**
     * @var int
     */
    private $resultB;

    /**
     * @var User
     */
    private $user;

    /**
     * @var TyperMatch
     */
    private $typerMatch;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set resultA
     *
     * @param integer $resultA
     * @return TyperTypeMatch
     */
    public function setResultA($resultA)
    {
        $this->resultA = $resultA;

        return $this;
    }

    /**
     * Get resultA
     *
     * @return integer 
     */
    public function getResultA()
    {
        return $this->resultA;
    }

    /**
     * Set resultB
     *
     * @param integer $resultB
     * @return TyperTypeMatch
     */
    public function setResultB($resultB)
    {
        $this->resultB = $resultB;

        return $this;
    }

    /**
     * Get resultB
     *
     * @return integer 
     */
    public function getResultB()
    {
        return $this->resultB;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getTyperMatch()
    {
        return $this->typerMatch;
    }

    /**
     * @param mixed $typerMatch
     */
    public function setTyperMatch($typerMatch)
    {
        $this->typerMatch = $typerMatch;
    }


}
