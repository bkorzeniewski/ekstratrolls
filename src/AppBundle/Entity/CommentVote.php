<?php
namespace  AppBundle\Entity;

use FOS\CommentBundle\Entity\Vote as BaseVote;
use FOS\CommentBundle\Model\SignedVoteInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class CommentVote extends BaseVote implements SignedVoteInterface
{
    protected $id;

    /**
     * Comment of this vote
     *
     * @var Comment
     */
    protected $comment;

    /**
     * Author of the vote
     *
     * @var User
     */
    protected $voter;

    /**
     * Sets the owner of the vote
     *
     * @param string $user
     */
    public function setVoter(UserInterface $voter)
    {
        $this->voter = $voter;
    }

    /**
     * Gets the owner of the vote
     *
     * @return UserInterface
     */
    public function getVoter()
    {
        return $this->voter;
    }


}