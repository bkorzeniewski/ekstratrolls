<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\Common\Collections\Criteria;

/**
 * TableFootball
 */
class TableFootball
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $lastModification;

    /**
     * @var League
     */
    private $league;

    /**
     * @var Season
     */
    private $season;

    /**
     * @var Queue
     */
    private $queue;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tableFootballClubs;

    /**
     * TableFootball constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->tableFootballClubs = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    /**
     * @return \DateTime
     */
    public function getLastModification()
    {
        return $this->lastModification;
    }

    /**
     * @param \DateTime $lastModification
     */
    public function setLastModification($lastModification = null)
    {
        $this->lastModification = null !== $lastModification ? $lastModification : new \DateTime();
    }

    /**
     * @return League
     */
    public function getLeague()
    {
        return $this->league;
    }

    /**
     * @param League $league
     */
    public function setLeague($league)
    {
        $this->league = $league;
    }

    /**
     * @return Season
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * @param Season $season
     */
    public function setSeason($season)
    {
        $this->season = $season;
    }

    /**
     * @return Queue
     */
    public function getQueue()
    {
        return $this->queue;
    }

    /**
     * @param Queue $queue
     */
    public function setQueue($queue)
    {
        $this->queue = $queue;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTableFootballClubs()
    {
        $criteria = Criteria::create()
            ->orderBy(array("points" => Criteria::DESC));

        return $this->tableFootballClubs->matching($criteria);
    }

    /**
     * Add objects
     *
     * @param \AppBundle\Entity\TableFootballClub $tableFootballClubs
     * @return Tag
     */
    public function addTableFootballClub(\AppBundle\Entity\TableFootballClub $tableFootballClubs)
    {
        $tableFootballClubs->setTableFootball($this);
        $this->tableFootballClubs[] = $tableFootballClubs;

        return $this;
    }

    /**
     * Remove objects
     *
     * @param \AppBundle\Entity\TableFootballClub $tableFootballClubs
     */
    public function removeTableFootballClub(\AppBundle\Entity\TableFootballClub $tableFootballClubs)
    {
        $this->tableFootballClubs->removeElement($tableFootballClubs);
    }

    function __toString()
    {
        return $this->league. ' - '.$this->season. ' - ' . $this->queue;
    }


}
