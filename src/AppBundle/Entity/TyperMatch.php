<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TyperMatch
 */
class TyperMatch
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateTime;

    /**
     * @var int
     */
    private $resultA;

    /**
     * @var int
     */
    private $resultB;

    /**
     * @var TyperTeam
     */
    private $teamA;

    /**
     * @var TyperTeam
     */
    private $teamB;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $typerTypeMatch;

    /**
     * @var TyperRoundMatches
     */
    private $typerRoundMatches;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set dateTime
     *
     * @param \DateTime $dateTime
     * @return TyperMatch
     */
    public function setDateTime($dateTime)
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    /**
     * Get dateTime
     *
     * @return \DateTime 
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * @return int
     */
    public function getResultA()
    {
        return $this->resultA;
    }

    /**
     * @param int $resultA
     */
    public function setResultA($resultA)
    {
        $this->resultA = $resultA;
    }

    /**
     * @return int
     */
    public function getResultB()
    {
        return $this->resultB;
    }

    /**
     * @param int $resultB
     */
    public function setResultB($resultB)
    {
        $this->resultB = $resultB;
    }

    /**
     * @return TyperTeam
     */
    public function getTeamA()
    {
        return $this->teamA;
    }

    /**
     * @param TyperTeam $teamA
     */
    public function setTeamA($teamA)
    {
        $this->teamA = $teamA;
    }

    /**
     * @return TyperTeam
     */
    public function getTeamB()
    {
        return $this->teamB;
    }

    /**
     * @param TyperTeam $teamB
     */
    public function setTeamB($teamB)
    {
        $this->teamB = $teamB;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTyperTypeMatch()
    {
        return $this->typerTypeMatch;
    }

    /**
     * @param \AppBundle\Entity\TyperTypeMatch $typerTypeMatch
     */
    public function addTyperTypeMatch(\AppBundle\Entity\TyperTypeMatch $typerTypeMatch)
    {
        $this->typerTypeMatch[] = $typerTypeMatch;
    }


    /**
     * Remove typerTypeMatch
     *
     * @param \AppBundle\Entity\TyperTypeMatch $typerTypeMatch
     */
    public function removeTyperTypeMatch(\AppBundle\Entity\TyperTypeMatch $typerTypeMatch)
    {
        $this->typerTypeMatch->removeElement($typerTypeMatch);
    }

    /**
     * @return TyperRoundMatches
     */
    public function getTyperRoundMatches()
    {
        return $this->typerRoundMatches;
    }

    /**
     * @param TyperRoundMatches $typerRoundMatches
     */
    public function setTyperRoundMatches($typerRoundMatches)
    {
        $this->typerRoundMatches = $typerRoundMatches;
    }





    /**
     * Constructor
     */
    public function __construct()
    {
        $this->typerTypeMatch = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString() {
        return (string)$this->getId();
    }

}
