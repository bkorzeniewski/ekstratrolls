<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Test
 */
class Test
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $tast;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tast
     *
     * @param string $tast
     * @return Test
     */
    public function setTast($tast)
    {
        $this->tast = $tast;

        return $this;
    }

    /**
     * Get tast
     *
     * @return string 
     */
    public function getTast()
    {
        return $this->tast;
    }
}
