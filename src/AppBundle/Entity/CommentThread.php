<?php

namespace  AppBundle\Entity;

use FOS\CommentBundle\Entity\Thread as BaseThread;

class CommentThread extends BaseThread
{
    /**
     * @var string $id
     *
     */
    protected $id;


    /**
     * @var string
     */
    private $type;

    /**
     * @var integer
     */
    private $typeId;


    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * @param int $typeId
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;
    }






}