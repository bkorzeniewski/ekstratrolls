<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TableFootballClub
 */
class TableFootballClub
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $matches;

    /**
     * @var int
     */
    private $won;

    /**
     * @var int
     */
    private $lost;

    /**
     * @var int
     */
    private $draw;

    /**
     * @var int
     */
    private $goalsScored;

    /**
     * @var int
     */
    private $goalsAgainst;

    /**
     * @var int
     */
    private $goalDifference;

    /**
     * @var int
     */
    private $points;

    /**
     * @var Club
     */
    private $club;

    /**
     * @var TableFootball
     */
    private $tableFootball;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set matches
     *
     * @param integer $matches
     * @return TableFootballClub
     */
    public function setMatches($matches)
    {
        $this->matches = $matches;

        return $this;
    }

    /**
     * Get matches
     *
     * @return integer
     */
    public function getMatches()
    {
        return $this->matches;
    }

    /**
     * Set won
     *
     * @param integer $won
     * @return TableFootballClub
     */
    public function setWon($won)
    {
        $this->won = $won;

        return $this;
    }

    /**
     * Get won
     *
     * @return integer
     */
    public function getWon()
    {
        return $this->won;
    }

    /**
     * Set lost
     *
     * @param integer $lost
     * @return TableFootballClub
     */
    public function setLost($lost)
    {
        $this->lost = $lost;

        return $this;
    }

    /**
     * Get lost
     *
     * @return integer
     */
    public function getLost()
    {
        return $this->lost;
    }

    /**
     * Set draw
     *
     * @param integer $draw
     * @return TableFootballClub
     */
    public function setDraw($draw = null)
    {

        $this->draw = null !== $draw ? $draw :
            $this->getMatches() - $this->getWon() - $this->getLost();

        return $this;
    }

    /**
     * Get draw
     *
     * @return integer
     */
    public function getDraw()
    {
        return $this->draw;
    }

    /**
     * Set goalsScored
     *
     * @param integer $goalsScored
     * @return TableFootballClub
     */
    public function setGoalsScored($goalsScored)
    {
        $this->goalsScored = $goalsScored;

        return $this;
    }

    /**
     * Get goalsScored
     *
     * @return integer
     */
    public function getGoalsScored()
    {
        return $this->goalsScored;
    }

    /**
     * Set goalsAgainst
     *
     * @param integer $goalsAgainst
     * @return TableFootballClub
     */
    public function setGoalsAgainst($goalsAgainst)
    {
        $this->goalsAgainst = $goalsAgainst;

        return $this;
    }

    /**
     * Get goalsAgainst
     *
     * @return integer
     */
    public function getGoalsAgainst()
    {
        return $this->goalsAgainst;
    }

    /**
     * Set goalDifference
     *
     * @param integer $goalDifference
     * @return TableFootballClub
     */
    public function setGoalDifference($goalDifference = null)
    {
        $this->goalDifference = null !== $goalDifference ? $goalDifference :
            $this->getGoalsScored() - $this->getGoalsAgainst();

        return $this;
    }

    /**
     * Get goalDifference
     *
     * @return integer
     */
    public function getGoalDifference()
    {
        return $this->goalDifference;
    }

    /**
     * Set points
     *
     * @param integer $points
     * @return TableFootballClub
     */
    public function setPoints($points = null)
    {
        $this->points = null !== $points ? $points :
            ($this->getWon() * 3) + $this->getDraw();

        return $this;
    }

    /**
     * Get points
     *
     * @return integer
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @return Club
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * @param Club $club
     */
    public function setClub($club)
    {
        $this->club = $club;
    }

    /**
     * @return mixed
     */
    public function getTableFootball()
    {
        return $this->tableFootball;
    }

    /**
     * @param mixed $tableFootball
     */
    public function setTableFootball($tableFootball)
    {
        $this->tableFootball = $tableFootball;
    }

    public function calculateValue(){
        $this->setDraw();
        $this->setGoalDifference();
        $this->setPoints();
    }

}
