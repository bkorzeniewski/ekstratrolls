<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Season
 */
class Season
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $date;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $leagues;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->leagues = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param string $date
     * @return Season
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return string 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Add leagues
     *
     * @param \AppBundle\Entity\League $leagues
     * @return Season
     */
    public function addLeague(\AppBundle\Entity\League $leagues)
    {
        $this->leagues[] = $leagues;

        return $this;
    }

    /**
     * Remove leagues
     *
     * @param \AppBundle\Entity\League $leagues
     */
    public function removeLeague(\AppBundle\Entity\League $leagues)
    {
        $this->leagues->removeElement($leagues);
    }

    /**
     * Get leagues
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLeagues()
    {
        return $this->leagues;
    }

    public function __toString()
    {
        return $this->date;
    }


}
