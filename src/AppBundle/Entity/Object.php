<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;


/**
 * Object
 */
class Object
{
    const IMAGE_TYPE = 'image';

    const YOUTUBE_TYPE = 'youtube';

    const FACEBOOK_TYPE = 'facebook';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $src;

    /**
     * @var string
     */
    private $videoSrc;

    /**
     * @var string
     */
    private $source;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $dateTransfer;

    /**
     * @var boolean
     */
    private $isWaiting;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $votes;


    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tags;


    /**
     * @var CommentThread;
     */
    private $commentThread;

    private $numberVote;

    /**
     * Constructor
     */
    public function __construct(array $properties = null)
    {
        $this->votes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();

        $this->createdAt = new \DateTime();
        $this->active = true;
        $this->isWaiting = true;

        if ($properties) {

            foreach ($properties as $key => $value) {

                if ("" != $value && "tags" != $key) {

                    $this->{$key} = $value;

                }

            }
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Object
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Object
     */
    public function setType($type)
    {
        if (!in_array($type, array(self::IMAGE_TYPE, self::YOUTUBE_TYPE, self::FACEBOOK_TYPE))) {
            throw new \InvalidArgumentException("Invalid status");
        }
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set src
     *
     * @param string $src
     * @return Object
     */
    public function setSrc($src)
    {

        if (strstr($src, 'youtu') || strstr($src, 'facebook')) {

            $this->setVideoSrc($src);

        } else {

            $this->type = self::IMAGE_TYPE;

            $regEx = "/^https?:\/\/(?:[a-z0-9\-]+\.)+[a-z]{2,6}(?:\/[^\/#?]+)+\.(?:jpe?g|gif|png)$/";

            if (preg_match($regEx, $src)) {

                $arr = explode(".", $src);

                $imageName = uniqid() . '.' . end($arr);


                $isAdded = file_put_contents($this->getUploadRootDir() . $imageName, file_get_contents($src));

                if ($isAdded) {

                    $this->src = $imageName;
                }

            } elseif (@getimagesize($src)) {
                $imageName = uniqid() . '.jpg';

                $isAdded = file_put_contents($this->getUploadRootDir() . $imageName, file_get_contents($src));

                if ($isAdded) {

                    $this->src = $imageName;
                }
            } else {

                $arr = explode("\\", $src);

                $this->src = end($arr);
            }


        }


        return $this;
    }

    /**
     * Get src
     *
     * @return string
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * Set videoSrc
     *
     * @param string $videoSrc
     * @return Object
     */
    public function setVideoSrc($videoSrc)
    {
        if (strstr($videoSrc, 'youtu')) {

            $this->type = self::YOUTUBE_TYPE;
            $arr = explode("=", $videoSrc);
            if (0 != count($arr)) {
                $this->videoSrc = end($arr);
            } else {
                $arr = explode("/", $videoSrc);
                $this->videoSrc = end($arr);
            }

            $this->saveImageVideo($this->videoSrc);

        } elseif (strstr($videoSrc, 'facebook')) {

            $this->type = self::FACEBOOK_TYPE;

            preg_match("/(\/videos\/|v\=)[0-9]+(\/|&)/", $videoSrc, $output);

            $idVideo = preg_replace("/(\/videos\/|\/)|(v\=|&)/", "", $output[0]);

            if (0 != strlen($idVideo)) {
                $this->videoSrc = $idVideo;
            } else {
                $this->videoSrc = $videoSrc;
            }

            $this->saveImageVideo($this->videoSrc);

        } else {

            $this->videoSrc = '';

        }

        return $this;
    }

    /**
     * Save image preview video
     *
     * @return string
     */
    private function saveImageVideo($srcVideo)
    {

        $imageUrl = null;

        if (self::YOUTUBE_TYPE === $this->type) {

            $imageUrl = 'http://img.youtube.com/vi/' . $srcVideo . '/0.jpg';


        } elseif (self::FACEBOOK_TYPE === $this->type) {

            $str = file_get_contents('http://graph.facebook.com/' . $srcVideo);
            $json = json_decode($str, true);
            if (count($json['format']) >= 3) {
                $imageUrl = $json['format'][2]['picture'];
            } else {
                $imageUrl = $json['format'][count($json['format']) - 1]['picture'];
            }


        }

        $imageName = uniqid() . '.jpg';

        $content = @file_get_contents($imageUrl);

        if ($content) {

            $isAdded = file_put_contents($this->getUploadRootDir() . $imageName, $content);

            if ($isAdded) {

                $this->src = $imageName;

            }

        } else {
            $this->src = "";
        }

    }

    /**
     * Get videoSrc
     *
     * @return string
     */
    public function getVideoSrc()
    {
        return $this->videoSrc;
    }

    /**
     * Set source
     *
     * @param string $source
     * @return Object
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Object
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set dateTransfer
     *
     * @param \DateTime $dateTransfer
     * @return Object
     */
    public function setDateTransfer($dateTransfer)
    {
        $this->dateTransfer = $dateTransfer;

        return $this;
    }

    /**
     * Get dateTransfer
     *
     * @return \DateTime
     */
    public function getDateTransfer()
    {
        return $this->dateTransfer;
    }

    /**
     * Set isWaiting
     *
     * @param boolean $isWaiting
     * @return Object
     */
    public function setIsWaiting($isWaiting)
    {
        if (0 === $isWaiting) {
            $this->dateTransfer = new \DateTime();
        }
        $this->isWaiting = $isWaiting;

        return $this;
    }

    /**
     * Get isWaiting
     *
     * @return boolean
     */
    public function getIsWaiting()
    {
        return $this->isWaiting;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Object
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add votes
     *
     * @param \AppBundle\Entity\ObjectVote $votes
     * @return Object
     */
    public function addVote(\AppBundle\Entity\ObjectVote $votes)
    {
        $this->votes[] = $votes;

        return $this;
    }

    /**
     * Remove votes
     *
     * @param \AppBundle\Entity\ObjectVote $votes
     */
    public function removeVote(\AppBundle\Entity\ObjectVote $votes)
    {
        $this->votes->removeElement($votes);
    }

    /**
     * Get votes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * @param mixed $numberVote
     */
    public function setNumberVote($numberVote = null)
    {
        if(null !==  $numberVote){
            $this->numberVote = $numberVote;
            return $this;
        }

        $votes = 0;
        foreach($this->getVotes() as $vote){
            $votes += $vote->getVote();
        }

        $this->numberVote = $votes;

        return $this;
    }


    /**
     * Get number votes
     *
     * @return int $number
     */
    public function getNumberVote()
    {

        return $this->numberVote  ;
    }



    /**
     * @return CommentThread
     */
    public function getCommentThread()
    {
        return $this->commentThread;
    }

    /**
     * @param CommentThread $commentThread
     */
    public function setCommentThread($commentThread)
    {
        $this->commentThread = $commentThread;
    }

    public function getNumComments(){

        return $this->getCommentThread() ? $this->getCommentThread()->getNumComments() : 0;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Object
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add tags
     *
     * @param \AppBundle\Entity\Tag $tags
     * @return Object
     */
    public function addTag(\AppBundle\Entity\Tag $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param \AppBundle\Entity\Tag $tags
     */
    public function removeTag(\AppBundle\Entity\Tag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }


    public function getAbsolutePath()
    {
        return null === $this->src ? null : $this->getUploadRootDir() . $this->src;
    }

    public function getWebPath()
    {
        return null === $this->src ? null : $this->getUploadDir() . $this->src;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . '/../../../web' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return '/uploads/images/';
    }

    public function removeFile()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    public function getUrlType()
    {

        return $this->type === self::IMAGE_TYPE ? $this->type : 'video';
    }

}
