<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Club
 */
class Club
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $footballers;

    /**
     * @var \AppBundle\Entity\League
     */
    private $league;

    /**
     * @var string
     */
    private $logo;


    /**
     * Image file
     *
     * @var File
     *
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    private $file;

    private $temp;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->footballers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Club
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add footballers
     *
     * @param \AppBundle\Entity\Footballer $footballers
     * @return Club
     */
    public function addFootballer(\AppBundle\Entity\Footballer $footballers)
    {
        $this->footballers[] = $footballers;

        return $this;
    }

    /**
     * Remove footballers
     *
     * @param \AppBundle\Entity\Footballer $footballers
     */
    public function removeFootballer(\AppBundle\Entity\Footballer $footballers)
    {
        $this->footballers->removeElement($footballers);
    }

    /**
     * Get footballers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFootballers()
    {
        return $this->footballers;
    }

    /**
     * Set league
     *
     * @param \AppBundle\Entity\League $league
     * @return Club
     */
    public function setLeague(\AppBundle\Entity\League $league = null)
    {
        $this->league = $league;

        return $this;
    }

    /**
     * Get league
     *
     * @return \AppBundle\Entity\League
     */
    public function getLeague()
    {
        return $this->league;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }




    public function getAbsoluteLogo()
    {
        return null === $this->logo
            ? null
            : $this->getUploadRootDir().'/'.$this->logo;
    }

    public function getWebLogo()
    {
        return null === $this->logo
            ? null
            : $this->getUploadDir().'/'.$this->logo;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory >mage where uploaded documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }


    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded uploads/clubs in the view.
        return 'uploads/clubs';
    }

    public function preUpload()
    {
        if (null !== $this->file) {
            // do whatever you want to generate a unique name
            $this->logo =  $this->getUniqueName().'.'.$this->file->guessExtension();
        }
    }


    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $logoName =  $this->getUniqueName().'.'.$this->file->guessExtension();
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $logoName
        );

        if (isset($this->temp)) {
            // delete the old logo
            if(file_exists($this->getUploadRootDir().'/'.$this->temp)){
                unlink($this->getUploadRootDir().'/'.$this->temp);
            }

            // clear the temp logo logo
            $this->temp = null;
        }
        $this->file = null;

        // set the logo property to the filename where you've saved the file
        $this->logo = $logoName;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }


    public function removeUpload()
    {
        if ($file = $this->getAbsoluteLogo()) {
            if(file_exists($file)){
                unlink($file);
            }

        }
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file)
    {
        $this->file = $file;

        // check if we have an old logo logo
        if (isset($this->logo)) {
            // store the old name to delete after the update
            $this->temp = $this->logo;
            $this->logo = null;
        } else {
            $this->logo = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }



    public function __toString()
    {
        return $this->name;
    }

    private function getUniqueName()
    {
        $patterns = array();
        $patterns[0] = '/\s+/';
        $patterns[1] = '/\.+/';
        $patterns[2] = '/\-+/';
        $replacements = array();
        $replacements[0] = '_';
        $replacements[1] = '';
        $replacements[2] = '_';

        $name  = preg_replace($patterns, $replacements,  $this->name);
        $name = $this->removePolishChar($name);
        $name = strtolower($name);

        return $name.'_'.uniqid();
    }

    private function removePolishChar($str)
    {
        $t = Array(
            //WIN
            "\xb9" => "a", "\xa5" => "A", "\xe6" => "c", "\xc6" => "C",
            "\xea" => "e", "\xca" => "E", "\xb3" => "l", "\xa3" => "L",
            "\xf3" => "o", "\xd3" => "O", "\x9c" => "s", "\x8c" => "S",
            "\x9f" => "z", "\xaf" => "Z", "\xbf" => "z", "\xac" => "Z",
            "\xf1" => "n", "\xd1" => "N",
            //UTF
            "\xc4\x85" => "a", "\xc4\x84" => "A", "\xc4\x87" => "c", "\xc4\x86" => "C",
            "\xc4\x99" => "e", "\xc4\x98" => "E", "\xc5\x82" => "l", "\xc5\x81" => "L",
            "\xc3\xb3" => "o", "\xc3\x93" => "O", "\xc5\x9b" => "s", "\xc5\x9a" => "S",
            "\xc5\xbc" => "z", "\xc5\xbb" => "Z", "\xc5\xba" => "z", "\xc5\xb9" => "Z",
            "\xc5\x84" => "n", "\xc5\x83" => "N",
            //ISO
            "\xb1" => "a", "\xa1" => "A", "\xe6" => "c", "\xc6" => "C",
            "\xea" => "e", "\xca" => "E", "\xb3" => "l", "\xa3" => "L",
            "\xf3" => "o", "\xd3" => "O", "\xb6" => "s", "\xa6" => "S",
            "\xbc" => "z", "\xac" => "Z", "\xbf" => "z", "\xaf" => "Z",
            "\xf1" => "n", "\xd1" => "N");

        return strtr($str,$t);
    }
}
