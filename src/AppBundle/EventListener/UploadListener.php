<?php
namespace AppBundle\EventListener;

use Oneup\UploaderBundle\Event\PostPersistEvent;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use AdminBundle\Form\ObjectType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity;
use AppBundle\Tag;

class UploadListener
{
    const OBJECT_TYPE = "object";
    const PROFILE_TYPE = "profile";
    protected $manager;
    protected $token_storage;

    public function __construct($token_storage, $manager)
    {
        $this->token_storage = $token_storage;
        $this->manager = $manager;
    }

    public function onUpload(PostPersistEvent $event)
    {
        $response = $event->getResponse();

        $request = $event->getRequest()->request;

        $src = $event->getFile()->getPathName();

        $user = $this->token_storage->getToken()->getUser();

        $type = $request->get('type');


        if (self::OBJECT_TYPE == $type) {

            $data = $request->get(self::OBJECT_TYPE);

            $entity = $this->saveUploadImageObject($data);

        } else {

            $data = $request->get(self::PROFILE_TYPE);
        }


        $entity->setSrc($src);

        $entity->setUser($user);

        $this->manager->persist($entity);

        $this->manager->flush();

        if (!empty($data['tags'])) {

            foreach($data['tags'] as $dataTag){

                $tagName = $dataTag['name'];

                $tag = $this->manager->getRepository('AppBundle:Tag')->findOneByName($tagName);

                if (!$tag) {

                    $tag = new Entity\Tag();

                    $tag->setName($tagName);

                    $tag->getObjects()->add($entity);

                    $this->manager->persist($tag);

                    $this->manager->flush();

                }

                $entity->getTags()->add($tag);

                $this->manager->flush();

            }
        }


        return $response;
    }

    private function saveUploadImageObject($data)
    {


        $entity = new Entity\Object($data);

        $entity->setType($entity::IMAGE_TYPE);

        return $entity;
    }


}