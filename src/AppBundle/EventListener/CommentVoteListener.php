<?php
namespace AppBundle\EventListener;

use FOS\CommentBundle\Event\VotePersistEvent;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use AdminBundle\Form\ObjectType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class CommentVoteListener
{
    protected $manager;
    protected $token_storage;

    public function __construct($token_storage, $manager)
    {
        $this->token_storage = $token_storage;
        $this->manager = $manager;
    }

    public function prePersistVote(VotePersistEvent $event)
    {
        $em = $this->manager;
        $vote = $event->getVote();



        $existingVote = $em->getRepository('AppBundle:CommentVote')->findOneBy(array(
            'voter' => $this->token_storage->getToken()->getUser(),
            'comment' => $vote->getComment(),
        ));

        if ($existingVote) {
            $em->remove($existingVote);
            $em->flush();
        }

    }



}