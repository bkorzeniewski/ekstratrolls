var Upload = Upload || {};

Upload.elemnt = {
    urlImage: '.upload-image-form-url',
    error: '.error',
    uploadList: '.qq-upload-list-selector',
    uploadRetry: '.qq-upload-retry-selector',
    uploadFail: '.qq-upload-fail',
    thumbnail: '.qq-thumbnail-selector',
    selectType: '.qq-select-type',
    cancel: '.qq-upload-cancel-selector',
    save: '.btn-save',
    addImageUrl: '.add-image-url',
    dropOver: '.qq-drop-over',
    dropEnter: '.qq-drop-enter',
    template: '#qq-template-uploader',
    uploader: '#fine-uploader',
    inputName: 'input[name$="[name]"]',
    inputSrc: 'input[name$="[src]"]',
    inputSource: 'input[name$="[source]"]',
    inputTags: 'input[name$="[tags]"]',
    inputDescription: 'textarea[name$="[description]"]',
    uploadForm: '.form-upload',
    nameForm: 'object',
    boxUploadForm: '.qq-upload-form',
    uploadFormHtml: ''
};

Upload.validate = {

    regExForUrlImage: /^https?:\/\/(?:[a-z0-9\-]+\.)+[a-z]{2,6}(?:\/[^\/#?]+)+\.(?:jpe?g|gif|png).*$/,
    
    checkUrl: function (url, reg) {
        if (reg.test(url))
            return true;
        else
            return false;
    },

    checkImageUrl: function (urlImg, callback) {
        var count = 0;
        var img = new Image();
        img.src = urlImg;

        if (typeof callback !== 'function') {
            callback = false;
        }

        if (img.height != 0) {
            callback();

        } else {
            var interval = setInterval(function () {
                if (img.height != 0) {
                    clearInterval(interval);
                    callback();
                }

                if (count >= 5) {
                    clearInterval(interval);
                    Upload.method.showErrorImageUrl();
                }
                count++;
            }, 100)
        }
    },

    checkDescription: function () {
        var $input = $(Upload.elemnt.inputDescription);
        var name = $input.val();

        if (name == '') {
            $input.next(Upload.elemnt.error).show();
            return false;
        } else {
            $input.next(Upload.elemnt.error).hide();
            return true
        }

    },
    validForm: function () {
        $(Upload.elemnt.uploadList + ' textarea').keyup(function () {
            Upload.validate.checkDescription();
        })
    },
};


Upload.data = {};

Upload.method = {

    showErrorImageUrl: function () {

        var input = $(Upload.elemnt.urlImage);
        $(Upload.elemnt.addImageUrl).fadeOut('hide', function () {

            if (input.val() != '')
                input.next(Upload.elemnt.error).show();
            else
                input.next(Upload.elemnt.error).hide();

        });

    },

    imageUrlFormShow: function (urlImg) {

        var template = $(Upload.elemnt.template).html();
        var contentForm = $(Upload.elemnt.uploadList, template).html();


        $(Upload.elemnt.uploadList).html(contentForm);

        $(Upload.elemnt.thumbnail).attr('src', urlImg);


        NProgress.start();

        $(Upload.elemnt.selectType).fadeOut('slow', function () {
            $(Upload.elemnt.uploadList).fadeIn('slow');

            NProgress.done();
        });

        $(Upload.elemnt.boxUploadForm).append(Upload.elemnt.uploadFormHtml);

        Upload.method.initTag(Upload.elemnt.boxUploadForm);

        $(Upload.elemnt.inputSource).val(urlImg);

        Upload.validate.validForm();
    },

    imageUrlFormHide: function (urlImg) {
        NProgress.start();

        $(Upload.elemnt.urlImage).val('');
        $(Upload.elemnt.inputSource).val(urlImg);
        urlImg = '';

        $(Upload.elemnt.addImageUrl).hide();
        $(Upload.elemnt.uploadList).fadeOut('slow', function () {
            $(Upload.elemnt.selectType).fadeIn('slow');
            $(this).children().remove();
            Upload.data = {};
            NProgress.done();
        });
    },

    imageFromUrl: function () {

        var urlImage = $(Upload.elemnt.urlImage).val();

        if (Upload.validate.checkUrl(urlImage, Upload.validate.regExForUrlImage)) {

            Upload.validate.checkImageUrl(urlImage, function () {


                $(Upload.elemnt.urlImage).next(Upload.elemnt.error).hide();

                $(Upload.elemnt.addImageUrl).fadeIn('slow', function () {

                    $(this).click(function () {
                        NProgress.start();

                        Upload.method.imageUrlFormShow(urlImage);

                        $(Upload.elemnt.save).click(function () {
                            $(Upload.elemnt.inputSrc).val(urlImage);
                            if (Upload.validate.checkDescription()) {
                                Upload.method.setData();
                                Upload.method.saveImageFromUrl(urlImage)
                            }


                        })

                        $(Upload.elemnt.cancel).click(function () {
                            Upload.method.imageUrlFormHide(urlImage);

                        })

                        NProgress.done();
                    })
                });
            })

        } else {

            Upload.method.showErrorImageUrl();
        }
    },
    getEndpoint: function () {
        return $(Upload.elemnt.uploader).attr('data-endpoint');
    },
    uploaderDragDrop: function () {

        $('.qq-uploader').isLoad(function () {
            var count = 0;
            $(this).bind({
                dragover: function () {
                    if (count < 1) {
                        $(Upload.elemnt.dropOver).show();
                        $(Upload.elemnt.dropEnter).hide();
                        count++;
                    }
                },
                dragleave: function () {
                    count = 0;
                    $(Upload.elemnt.dropOver).hide();
                    $(Upload.elemnt.dropEnter).show();
                }
            });

        });
    },

    saveUploadImage: function () {
        $(Upload.elemnt.save).click(function () {
            if (Upload.validate.checkDescription()) {
                Upload.method.setData();
                up.fineUploader('setParams', Upload.data);
                $(Upload.elemnt.uploader).fineUploader('uploadStoredFiles');
            }

        });
    },
    setData: function () {
        Upload.data = {};
        var $inputs = $('form :input');
        Upload.data['type'] = $('form').attr('name');
        $inputs.each(function () {
            Upload.data[this.name] = $(this).val();
        });
        console.log(Upload.data);
    },
    saveImageFromUrl: function (urlImage) {
        NProgress.start();
        $.ajax({
            url: Routing.generate('object_new'),
            type: 'POST',
            data: Upload.data,
            success: function (response) {
                Upload.method.retryAddImage(urlImage);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                Upload.method.retryUpload(function () {
                    Upload.method.saveImageFromUrl(urlImage);
                })
            }
        });
    },

    retryUpload: function (callback) {
        swal({
            title: "Obrazek nie został dodany ",
            text: "Wystąpił błąd podczas dodawania obrazka",
            type: "error",
            closeOnConfirm: true,
            showCancelButton: true,
            cancelButtonText: "Zamknij",
            confirmButtonText: "Spróbuj jeszcze raz",
            confirmButtonColor: "#DD6B55"
        }, function (isConfirm) {
            if (typeof callback !== 'function') {
                callback = false;
            }
            if (callback) {
                callback(isConfirm);
            }
        })
    },

    retryAddImage: function (urlImage) {
        NProgress.done();
        swal({
            title: "Obrazek został dodany",
            text: "Czy chcesz dodać nowy Obrazek?",
            type: "success",
            showCancelButton: true,
            confirmButtonText: "Tak",
            cancelButtonText: "Nie",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                Upload.method.imageUrlFormHide(urlImage);
                swal.close();
            } else {
                window.location.replace(Routing.generate('object_index'));
            }
        });
    },

    setUploadFormHtml: function () {
        Upload.elemnt.uploadFormHtml = $(Upload.elemnt.uploadForm).html();
        $(Upload.elemnt.uploadForm).remove();

    },

    initTag: function (type) {

        ///$(type + ' ' +Upload.elemnt.inputDescription);

        var $inputTag = $(type + ' input[data-role=tagsinput]');

        var $inputTagsinput = $(type + ' .bootstrap-tagsinput input');

        $inputTag.tagsinput({
            addOnBlur:false
        });



        $(type + ' .bootstrap-tagsinput input').autocomplete({
           serviceUrl: Routing.generate('tag_get'),
            onSelect: function (suggestion) {
                $inputTag.tagsinput('add',suggestion.value);
                $(this).val('');
            },
            minChars: 3,
            width: 400,
            preventBadQueries: false,
            noCache: true,
            onSearchStart: function (query) {
                var width = $(type + ' .bootstrap-tagsinput').width();
               var left = $(type + ' .bootstrap-tagsinput input').position().left;


                $('.autocomplete-suggestions').css('width', width-left+22+'px');
            }
        });


        $(type + ' .bootstrap-tagsinput input').get(0).style.setProperty( 'width', '', 'important' );

        var count = 0;
        $inputTag.on('beforeItemAdd', function (event) {
            var tag = event.item;

            console.log(event.options);
            $(this).after('<input type="hidden" id="object_tags_' + count + '_name" class="inputTag" name="object[tags][' + count + '][name]" value="' + tag + '"  />');

            count++;

            console.log(tag);
        });


        $inputTag.on('beforeItemRemove', function(event) {
            var tag = event.item;
            $("input.inputTag[value="+tag+"]").remove();

        });

    }
}


Upload.init = function () {

    // fine uploader image from url
    $(Upload.elemnt.urlImage).on('input', function () {

        Upload.method.imageFromUrl();

    })


    Upload.method.setUploadFormHtml();


    // fine uploader drag and drop event
    Upload.method.uploaderDragDrop()


}
// setting fine uploader
var up = $(Upload.elemnt.uploader).fineUploader({
    template: 'qq-template-uploader',
    request: {
        endpoint: Upload.method.getEndpoint()
    },
    thumbnails: {
        placeholders: {
            waitingPath: '/source/placeholders/waiting-generic.png',
            notAvailablePath: '/source/placeholders/not_available-generic.png'
        }
    },
    validation: {
        allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
    },
    autoUpload: false,
    multiple: false,
    callbacks: {
        onSubmit: function (id, name) {
            NProgress.start();
            $(Upload.elemnt.thumbnail).isLoad('src', function () {
                $(Upload.elemnt.selectType).fadeOut('slow', function () {
                    $(Upload.elemnt.boxUploadForm).append(Upload.elemnt.uploadFormHtml);

                    Upload.method.initTag(Upload.elemnt.boxUploadForm);

                    $(Upload.elemnt.uploadList).fadeIn('slow');

                    Upload.method.saveUploadImage();

                });
                Upload.validate.validForm();
                NProgress.done();
            });
        },
        onCancel: function (id, name) {
            NProgress.start();
            $(Upload.elemnt.uploadList).fadeOut('slow', function () {
                $(Upload.elemnt.selectType).fadeIn('slow', function () {
                });
            });
            NProgress.done();
        },
        onTotalProgress: function (totalUploadedBytes, totalBytes) {
            var progress = totalUploadedBytes / totalBytes;
            if (progress != 0)
                NProgress.set(progress);
        },
        onComplete: function (id, name, responseJSON, xhr) {
            if (responseJSON.success) {
                Upload.method.retryAddImage("");
            } else {
                Upload.method.retryUpload(function(isConfirm){
                    if (isConfirm) {
                        var files = up.fineUploader('getUploads', 'id');
                        var id = files[files.length - 1].id;
                        up.fineUploader('retry', id);
                    }else{
                        Upload.method.imageUrlFormHide("");
                    }
                })

            }

        }

    },
    showMessage: function (message) {
        sweetAlert("Oops...", message, "error");
    },
    messages: {
        sizeError: '{file} jest zbyt duży! Zdjęci muszi być ograniczone do {sizeLimit} lub mniejsze.',
        tooManyFilesError: 'Może upościć tylko jedenen obrazek.',
        noFilesError: 'Nie wybrałeś obrazka.',
        typeError: '{file} ma nieprawidłowe rozszerzenie. Obrazek powinno mieć rozszerzenie : {extensions}.'
    }

});


// Uploader
Upload.init();

var Video = Video || {};

Video.elemnt = {
    addCon: '.video-con',
    addBtn: '.add-video',
    urlVideo: '.video-url',
    formConVideo: '.video-con-form',
    formVideo: '.video-form',
    iframe: 'iframe',
    facebook: '#facebook ',
    youtube: '#youtube '
}

Video.validate = {
    regExVideo: /^(https?\:\/\/)?(www\.)?(youtube\.com|youtu\.?be)\/.+$/,
    $fbUrlCheck: /^(https?:\/\/)?(www\.)?facebook.com\/[a-zA-Z0-9(\.\?)?]/,
    checkDescription: function (type) {
        var $input = $(type + Upload.elemnt.inputDescription);
        var name = $input.val();

        if (name == '') {
            $input.next(Upload.elemnt.error).show();
            return false;
        } else {
            $input.next(Upload.elemnt.error).hide();
            return true
        }

    }

}

Video.method = {

    setData: function (type) {
        Upload.data = {};
        var $inputs = $(type + 'form :input');
        $inputs.each(function () {
            Upload.data[this.name] = $(this).val();
        });
    },


    videoFormShow: function (url, type) {

        NProgress.start();
        var urlIframe = '';

        if (type == Video.elemnt.youtube) {
            urlIframe = 'https://www.youtube.com/embed/' + Video.method.getYoutubeIdFromUrl(url)
        } else {
            urlIframe = 'https://www.facebook.com/v2.3/plugins/video.php?app_id=113869198637480&channel=https%3A%2F%2Fs-static.ak.facebook.com%2Fconnect%2Fxd_arbiter%2FR_qmi4A5CC2.js%3Fversion%3D41%23cb%3Df2acc793f4%26domain%3Ddevelopers.facebook.com%26origin%3Dhttps%253A%252F%252Fdevelopers.facebook.com%252Ff23e028d48%26relation%3Dparent.parent&container_width=588&href=' + encodeURIComponent(url) + '&locale=pl_PL&sdk=joey&width=500';

        }

        if ('' != urlIframe) {
            $(type + Video.elemnt.iframe).attr('src', urlIframe);
        }

        $(type + Video.elemnt.addCon).fadeOut('slow', function () {
            if ($(type + Video.elemnt.formVideo).children().length == 0) {
                $(type + Video.elemnt.formVideo).append(Upload.elemnt.uploadFormHtml);

                Upload.method.initTag(type);

                Video.method.getVideoDetails(url, type);
            }

            $(type + Upload.elemnt.save).click(function () {

                $(type + Upload.elemnt.inputSrc).val(url);
                if (Video.validate.checkDescription(type)) {
                    NProgress.start();
                    Video.method.setData(type);
                    Video.method.saveVideo(type);
                }
            })

            $(type + Upload.elemnt.cancel).click(function () {
                $(type + Video.elemnt.iframe).attr('src', '');
                Video.method.videoUrlFormHide(type)

            })
            $(type + Video.elemnt.formConVideo).fadeIn('slow');
            NProgress.done();
        })
    },
    videoUrl: function (type) {
        var urlVideo = $(type + Video.elemnt.urlVideo).val();
        var flag = false;
        var regEx;
        if (type == Video.elemnt.youtube) {
            regEx = Video.validate.regExVideo;
        } else {
            regEx = Video.validate.$fbUrlCheck;
        }
        if (Upload.validate.checkUrl(urlVideo, regEx)) {
            $(type + Video.elemnt.urlVideo).next(Upload.elemnt.error).hide();
            $(type + Video.elemnt.addBtn).fadeIn('slow', function () {
                $(this).one("click", function () {

                    Video.method.videoFormShow(urlVideo, type);
                })
            })

        } else {
            Video.method.showErrorVideoUrl(type);
        }
    },
    showErrorVideoUrl: function (type) {

        var input = $(type + Video.elemnt.urlVideo);
        $(type + Video.elemnt.addBtn).fadeOut('fast', function () {
            if (input.val() != '')
                input.next(Upload.elemnt.error).show();
            else
                input.next(Upload.elemnt.error).hide();

        });


    },
    videoUrlFormHide: function (type) {
        NProgress.start();

        $(type + Video.elemnt.urlVideo).val('');
        $(type + Video.elemnt.addBtn).hide();
        $(type + Video.elemnt.formConVideo).fadeOut('slow', function () {
            $(type + Video.elemnt.formVideo).empty();
            $(type + Video.elemnt.addCon).fadeIn('slow');
            NProgress.done();
        });
    },
    getYoutubeIdFromUrl: function (url) {
        var arr = url.split("=");
        if (arr.length > 0) {
            return arr[arr.length - 1];
        } else {
            var arr = url.split("/");
            return arr[arr.length - 1];
        }
    },
    saveVideo: function (type) {
        $.ajax({
            url: $('form[name="' + Upload.elemnt.nameForm + '"]').attr('action'),//Routing.generate('object_new'),
            type: 'POST',
            data: Upload.data,
            success: function (response) {
                NProgress.done();
                swal({
                    title: "Film został dodany",
                    text: "Czy chcesz dodać nowy film?",
                    type: "success",
                    showCancelButton: true,
                    confirmButtonText: "Tak",
                    cancelButtonText: "Nie",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                        $(type + Video.elemnt.iframe).attr('src', '');
                        Video.method.videoUrlFormHide(type);
                        swal.close();
                    } else {
                        window.location.replace(Routing.generate('object_index'));
                    }
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                Video.method.retryUpload(function () {
                    Video.method.saveVideo(type);
                })
            }
        });
    },
    retryUpload: function (callback) {
        swal({
            title: "Film nie został dodany ",
            text: "Wystąpił błąd podczas dodawania filmu",
            type: "error",
            closeOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: "Spróbuj jeszcze raz",
            confirmButtonColor: "#DD6B55"
        }, function () {
            if (typeof callback !== 'function') {
                callback = false;
            }
            if (callback) {
                callback();
            }
        })
    },

    getVideoDetails: function (url, type) {
        if (type == Video.elemnt.facebook) {
            var str = url.match(/(\/videos\/|v\=)[0-9]+(\/|&)/)[0];
            console.log(str);
            var idFacebook = str.replace(new RegExp('(\/videos\/|\/)|(v\=|&)', 'g'), '');
            console.log(idFacebook);
            $.getJSON('http://graph.facebook.com/' + idFacebook, function (data) {
                console.log(data.description);
                // var name = data.name;
                $(type + Upload.elemnt.inputDescription).val(data.name);
                $(type + Upload.elemnt.inputSource).val(url)
                $(type + Upload.elemnt.inputDescription).val(data.description);

            });

        } else {
            $(type + Upload.elemnt.inputSource).val(url)
        }

    }
}

$(Video.elemnt.urlVideo).on('input', function () {
    var type = $(this).closest('.tab-pane').attr('id');
    Video.method.videoUrl('#' + type + ' ');
})