/*
 *  Functions jQuery
 */
(function ($) {

    /* Check load elemnt */
    $.fn.isLoad = function (attr, callback) {
        if (typeof callback === 'undefined') {
            callback = attr;
        }
        if (typeof callback !== 'function') {
            callback = false;
        }
        if (callback) {
            var element = this;
            var interval = setInterval(function () {
                if (typeof attr !== 'function') {
                    if ($(element.selector).attr(attr) !== undefined) {
                        clearInterval(interval);
                        callback();
                    }
                } else if ($(element.selector).length > 0) {
                    clearInterval(interval);
                    callback();
                }
            }, 100);
        }
        return this;
    };


    //social button

    $.fn.socialButton = function () {
        this.each(function () {
            var $el = $(this);
            var url = encodeURIComponent($el.data('url'));
            var facebook = $el.find('.facebook');
            var twitter = $el.find('.twitter');
            var google = $el.find('.google');

            facebook.on('click', function(){
                popupwindow('https://www.facebook.com/sharer/sharer.php?u=' + url + '&t=' + url, "", 500, 500);
            });
            twitter.on('click', function(){
                popupwindow('https://twitter.com/intent/tweet?text=' + encodeURIComponent(document.title) + ':%20' + url, "", 500, 500);
            });

            google.on('click', function(){
                popupwindow('https://plus.google.com/share?url=' + url, "", 500, 500);
            })

            var popupwindow = function(url, title, w, h) {
                var left = (screen.width/2)-(w/2);
                var top = (screen.height/2)-(h/2);
                return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
            }
        });
        return this;
    };

    $('.social-group').socialButton();

    $('.show-result').on('click', function(){
        $(this).hide().closest('.module').find('.result').show().prev().hide();
    });

    $('.questionnaire').submit(function(e){
        var $answers = $(this).find('.answers');
        var $submitButton = $(this).find('button[type="submit"]');
        var $resultButton = $(this).find('.show-result');
        if($($answers).is(":visible")){
            if($(this).hasClass('must-login')){
                alert('Musisz się zalogować')
                e.preventDefault();
                return;
            }
            $(this).submit();
        }else{
            $resultButton.show().closest('.module').find('.result').hide().prev().show();
            $submitButton.blur();
            e.preventDefault();
            return;
        }
    })


})(jQuery);

var Vote = Vote || {};

Vote.element = {
    vote: $('.vote'),
    voteUp: $('.vote-up'),
    voteDown: $('.vote-down'),
    voteCount: $('.vote-count'),
    routeAjax: Routing.generate('set_vote')
};

Vote.method = {
    getObjectId: function (object) {
        var objectId = object.closest('article').data('objectId');
        return objectId;
    },
    getDataVote: function (object) {
        var value = null;
        if (object.is(Vote.element.voteUp)) {
            value = 1;
        } else if (object.is(Vote.element.voteDown)) {
            value = -1;
        }
        return {'vote[object]': Vote.method.getObjectId(object), 'vote[value]': value}
    },
    sendAjaxPostVote: function (object) {
        $.ajax({
            url: Vote.element.routeAjax,
            type: 'POST',
            data: Vote.method.getDataVote(object),
            success: function (response) {
                for (var i = 0; i < response.data.length; i++) {
                    if (response.data[i].status == "error") {
                        if (response.data[i].info = "user_not_found") {
                            alert('musisz się zalogować');
                            break;
                        }
                    } else if (response.data[i].status == "success") {
                        var voteCount = object.closest('article').find(Vote.element.voteCount).text();
                        if (response.data[i].info == "add_vote") {

                            if (response.data[i].vote == 1) {
                                object.addClass('active');
                                object.next().removeClass('active');
                                object.closest('article').find(Vote.element.voteCount).text(++voteCount);
                            } else if (response.data[i].vote == -1) {
                                object.addClass('active');
                                object.prev().removeClass('active');
                                object.closest('article').find(Vote.element.voteCount).text(--voteCount);
                            }
                        } else if (response.data[i].info == "remove_vote") {
                            object.removeClass('active');
                        }

                        break;
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.error(xhr);
            }
        });
    }

}
Vote.init = function () {
    Vote.element.voteUp.click(function () {
        Vote.method.sendAjaxPostVote($(this));
        this.blur();
    })

    Vote.element.voteDown.click(function () {
        Vote.method.sendAjaxPostVote($(this));
        this.blur();
    })
}

Vote.init();

var Comment = Comment || {};

Comment.element = {
    commentPost: $('.comment-post'),
    textarea: $('.comment-textarea'),
    save: $('.comment-save'),
    list: $('.comment-list'),
    routeAjax: Routing.generate('comment_new')
}

Comment.method = {
    getObjectId: function (object) {
        var objectId = object.closest(Comment.element.commentPost).data('objectId');
        return objectId;
    },
    getDataVote: function (object) {
        var value = Comment.element.textarea.val();
        return {'comment[object]': Comment.method.getObjectId(object), 'comment[value]': value};
    },
    sendAjaxPostComment: function (object) {
        $.ajax({
            url: Comment.element.routeAjax,
            type: 'POST',
            data: Comment.method.getDataVote(object),
            success: function (response) {
                for (var i = 0; i < response.data.length; i++) {
                    if (response.data[i].status == "error") {
                        if (response.data[i].info = "user_not_found") {
                            alert('musisz się zalogować');
                            break;
                        }
                    } else if (response.data[i].status == "success") {
                        if (response.data[i].info == "add_comment") {

                            Comment.method.addComment($.parseJSON(response.data[i].comment));
                            break;
                        }


                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr);
            }
        });
    },
    addComment: function(comment){

        var htmlComment = '<div class="comment-entry"> ' +
            '<div class="avatar"> <img src="'+comment.user.thumbAvatar+'"> </div> ' +
            '<div class="comment-box"> ' +
            '<div class="info"> '+comment.user.username+' <span class="data-add">'+Comment.method.dateTimeConverter(comment.dateAdd.timestamp)+'</span> </div> ' +
            '<div class="content"> '+comment.text +'</div> ' +
            '</div> </div>'

        Comment.element.textarea.val('');
        Comment.element.list.prepend(htmlComment);
    },
    dateTimeConverter: function(UNIX_timestamp){
        var a = new Date(UNIX_timestamp * 1000);
        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var year = a.getFullYear();
        var month = months[a.getMonth()];
        var date = a.getDate();
        var hour = a.getHours();
        var min = a.getMinutes();
        var sec = a.getSeconds();
        var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
        return time;
    }
}

Comment.init = function () {
    Comment.element.save.click(function () {
        Comment.method.sendAjaxPostComment($(this));
        this.blur();
    })


}



