<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\TyperTypeMatch;
use AppBundle\Form\TyperTypeMatchType;

/**
 * TyperTypeMatch controller.
 *
 */
class TyperTypeMatchController extends Controller
{
    /**
     * Lists all TyperTypeMatch entities.
     *
     */
    public function indexAction(Request $request, $numberRound)
    {
        $em = $this->getDoctrine()->getManager();

        $typerRoundMatches = $em->getRepository('AppBundle:TyperRoundMatches')->findOneByNumberRound($numberRound);

        $user = $this->get('security.context')->getToken()->getUser();


        $form = $this->createForm('AppBundle\Form\TyperTypeMatchType', new TyperTypeMatch());
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $requestTyperMatch = $request->request->get('typer_type_match');

            $typerMatch = $em->getRepository('AppBundle:TyperMatch')->find($requestTyperMatch['typerMatch']);

            $typerTypeMatch = $em->getRepository('AppBundle:TyperTypeMatch')->findOneBy(
                array('user' => $user, 'typerMatch' => $typerMatch)
            );

            if (!$typerTypeMatch) {
                $typerTypeMatch = new TyperTypeMatch();
            }
            $typerTypeMatch->setTyperMatch($typerMatch);
            $typerTypeMatch->setUser($user);
            $typerTypeMatch->setResultA($requestTyperMatch['resultA']);
            $typerTypeMatch->setResultB($requestTyperMatch['resultB']);
            $em->persist($typerTypeMatch);
            $em->flush();
        }

        $typerMatchForms = array();

        foreach ($typerRoundMatches->getTyperMatches() as $typerMatch) {
            $typerTypeMatch = $em->getRepository('AppBundle:TyperTypeMatch')->findOneBy(
                array('user' => $user, 'typerMatch' => $typerMatch)
            );

            if (!$typerTypeMatch) {
                $typerTypeMatch = new TyperTypeMatch();
                $typerTypeMatch->setTyperMatch($typerMatch);
            }
            $form = $this->createForm('AppBundle\Form\TyperTypeMatchType', $typerTypeMatch);

            $typerMatchForms[] = array('form' => $form->createView(), 'typerMatch' => $typerMatch, 'userType' => $typerTypeMatch);
        }


        return $this->render('typertypematch/index.html.twig', array(
            'typerMatchForms' => $typerMatchForms
        ));
    }

    /**
     * Creates a new TyperTypeMatch entity.
     *
     */
    public function newAction(Request $request)
    {
        $typerTypeMatch = new TyperTypeMatch();
        $form = $this->createForm('AppBundle\Form\TyperTypeMatchType', $typerTypeMatch);
        $form->handleRequest($request);

        $user = $this->get('security.context')->getToken()->getUser();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $typerTypeMatch->setUser($user);
            $em->persist($typerTypeMatch);
            $em->flush();

            return $this->redirectToRoute('typertypematch_show', array('id' => $typerTypeMatch->getId()));
        }

        return $this->render('typertypematch/new.html.twig', array(
            'typerTypeMatch' => $typerTypeMatch,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TyperTypeMatch entity.
     *
     */
    public function showAction(TyperTypeMatch $typerTypeMatch)
    {
        $deleteForm = $this->createDeleteForm($typerTypeMatch);

        return $this->render('typertypematch/show.html.twig', array(
            'typerTypeMatch' => $typerTypeMatch,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TyperTypeMatch entity.
     *
     */
    public function editAction(Request $request, TyperTypeMatch $typerTypeMatch)
    {
        $deleteForm = $this->createDeleteForm($typerTypeMatch);
        $editForm = $this->createForm('AppBundle\Form\TyperTypeMatchType', $typerTypeMatch);
        $editForm->handleRequest($request);

        $user = $this->get('security.context')->getToken()->getUser();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $typerTypeMatch->setUser($user);
            $em->persist($typerTypeMatch);
            $em->flush();

            return $this->redirectToRoute('typertypematch_edit', array('id' => $typerTypeMatch->getId()));
        }

        return $this->render('typertypematch/edit.html.twig', array(
            'typerTypeMatch' => $typerTypeMatch,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a TyperTypeMatch entity.
     *
     */
    public function deleteAction(Request $request, TyperTypeMatch $typerTypeMatch)
    {
        $form = $this->createDeleteForm($typerTypeMatch);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typerTypeMatch);
            $em->flush();
        }

        return $this->redirectToRoute('typertypematch_index');
    }

    /**
     * Creates a form to delete a TyperTypeMatch entity.
     *
     * @param TyperTypeMatch $typerTypeMatch The TyperTypeMatch entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TyperTypeMatch $typerTypeMatch)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('typertypematch_delete', array('id' => $typerTypeMatch->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
