<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\TyperRoundMatches;
use AppBundle\Entity\TyperMatch;

/**
 * TyperRoundMatches controller.
 *
 */
class TyperRoundMatchesController extends Controller
{
    /**
     * Lists all TyperRoundMatches entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $typerRoundMatches = $em->getRepository('AppBundle:TyperRoundMatches')->findAll();

        return $this->render('typerroundmatches/index.html.twig', array(
            'typerRoundMatches' => $typerRoundMatches,
        ));
    }

    /**
     * Creates a new TyperRoundMatches entity.
     *
     */
    /*
    public function newAction(Request $request)
    {
        $typerRoundMatch = new TyperRoundMatches();

        for ($i = 0; $i < 8; $i++) {
            $typerMatch = new TyperMatch();
            $typerRoundMatch->addTyperMatch($typerMatch);
        }


        $form = $this->createForm('AppBundle\Form\TyperRoundMatchesType', $typerRoundMatch);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typerRoundMatch);
            $em->flush();

            return $this->redirectToRoute('typerroundmatches_show', array('id' => $typerRoundMatch->getId()));
        }

        return $this->render('typerroundmatches/new.html.twig', array(
            'typerRoundMatch' => $typerRoundMatch,
            'form' => $form->createView(),
        ));
    }
    */

    /**
     * Finds and displays a TyperRoundMatches entity.
     *
     */
    /*
       public function showAction(TyperRoundMatches $typerRoundMatch)
     {
         $deleteForm = $this->createDeleteForm($typerRoundMatch);

         return $this->render('typerroundmatches/show.html.twig', array(
             'typerRoundMatch' => $typerRoundMatch,
             'delete_form' => $deleteForm->createView(),
         ));
     }
    */

    /**
     * Displays a form to edit an existing TyperRoundMatches entity.
     *
     */
    public function editAction(Request $request, TyperRoundMatches $typerRoundMatch)
    {
        $sizeTyperMatches = count($typerRoundMatch->getTyperMatches());
        for ($i = 0; $i < (8 - $sizeTyperMatches); $i++) {
            $typerMatch = new TyperMatch();
            $typerMatch->setTyperRoundMatches($typerRoundMatch);
            $typerRoundMatch->addTyperMatch($typerMatch);
        }

        // $deleteForm = $this->createDeleteForm($typerRoundMatch);
        $editForm = $this->createForm('AppBundle\Form\TyperRoundMatchesType', $typerRoundMatch);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typerRoundMatch);
            $em->flush();

            return $this->redirectToRoute('typerroundmatches_edit', array('id' => $typerRoundMatch->getId()));
        }

        return $this->render('typerroundmatches/edit.html.twig', array(
            'typerRoundMatch' => $typerRoundMatch,
            'edit_form' => $editForm->createView(),
            // 'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a TyperRoundMatches entity.
     *
     */
    /*
    public function deleteAction(Request $request, TyperRoundMatches $typerRoundMatch)
     {
         $form = $this->createDeleteForm($typerRoundMatch);
         $form->handleRequest($request);

         if ($form->isSubmitted() && $form->isValid()) {
             $em = $this->getDoctrine()->getManager();
             $em->remove($typerRoundMatch);
             $em->flush();
         }

         return $this->redirectToRoute('typerroundmatches_index');
     }
    */

    /**
     * Creates a form to delete a TyperRoundMatches entity.
     *
     * @param TyperRoundMatches $typerRoundMatch The TyperRoundMatches entity
     *
     * @return \Symfony\Component\Form\Form The form
     */

    /*
     private function createDeleteForm(TyperRoundMatches $typerRoundMatch)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('typerroundmatches_delete', array('id' => $typerRoundMatch->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
    */
}
