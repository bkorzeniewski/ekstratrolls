<?php

namespace AppBundle\Controller;

use Symfony\Bridge\Doctrine\Tests\Fixtures\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

use AppBundle\Entity\Comment;

/**
 * Comment controller.
 *
 */
class CommentController extends Controller
{

    /**
     * Creates a new Comment entity.
     *
     */
    public function newAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {

            $responseData = array();

            $em = $this->getDoctrine()->getManager();

            $commentValue = $request->request->get('comment');

            try {

                $value = $commentValue['value'];

                if ($value == "" || !isset($value)) {

                    $responseData[] = array(
                        'status' => "error",
                        'info' => 'value',
                    );

                }

            } catch (\Exception $e) {

                $responseData[] = array(
                    'status' => "error",
                    'info' => 'value',
                );

            }

            try {

                $objectId = (int)$commentValue['object'];

            } catch (\Exception $e) {
                $responseData[] = array(
                    'status' => "error",
                    'info' => 'objectId',
                );
            }


            $user = $this->get('security.context')->getToken()->getUser();

            if ($user == 'anon.') {
                $user = null;
                $responseData[] = array(
                    'status' => "error",
                    'info' => 'user_not_found',
                );
            }


            $object = $em->getRepository('AppBundle:Object')->find($objectId);

            if (!$object) {
                $responseData[] = array(
                    'status' => "error",
                    'info' => 'object_not_found',
                );
            }

            if ($user && $object && ($value != "" || isset($value))) {

                $comment = new Comment();
                $comment->setUser($user)->setObject($object)->setText($value);
                $em->persist($comment);
                $em->flush();

                $user = $comment->getUser();

                $cacheManager = $this->container->get('liip_imagine.cache.manager');
                $srcPath = $cacheManager->getBrowserPath($user->getWebPath(), 'thumb_avatar');

                $user->setAvatar($srcPath);



                $encoders = new JsonEncoder();
                $normalizer = new ObjectNormalizer();


                $normalizer->setCircularReferenceHandler(function ($object) {
                    return $object;
                });

                $normalizer->setIgnoredAttributes(array(
                    'object',
                    'absolutePath',
                    'accountNonExpired',
                    'accountNonLocked',
                    'comments',
                    'confirmationToken',
                    'credentialsExpired',
                    'credentialsNonExpired',
                    'email',
                    'emailCanonical',
                    'enabled',
                    'expired',
                    'facebookAccessToken',
                    'facebookId',
                    'firstName',
                    'groupNames',
                    'groups',
                    'lastLogin',
                    'lastName',
                    'locked',
                    'objects',
                    'password',
                    'passwordRequestedAt',
                    'plainPassword',
                    'roles',
                    'salt',
                    'superAdmin',
                    'usernameCanonical',
                    'votes',
                    'webPath', ));

                $user->thumbAvatar = $srcPath;
                $comment->setUser($user);

                $serializer = new Serializer(array($normalizer), array($encoders));

                $responseData[] = array(
                    'status' => "success",
                    'info' => 'add_comment',
                    'comment' => $serializer->serialize($comment, 'json'),
                );

            }


            $json = json_encode(array(
                'data' => $responseData
            ));


            $response = new Response($json);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }

        return null;


    }

}
