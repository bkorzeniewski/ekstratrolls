<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Object;
use AppBundle\Entity\Tag;
use Doctrine\Common\Collections\ArrayCollection;

use  AppBundle\DQL\ObjectFind;


/**
 * Object controller.
 *
 */
class ObjectController extends Controller
{
    const ITEM_PER_PAGE = 2;
    /**
     * Lists all Object entities.
     *
     */
    public function indexAction($page = 1)
    {
        $em = $this->get('doctrine.orm.entity_manager');

        $dql   = ObjectFind::getFindAllToMain();

        $query = $em->createQuery($dql);

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate($query, $page, self::ITEM_PER_PAGE);

        // parameters to template

        return $this->render('object/index.html.twig', array(
            'pagination' => $pagination,
        ));
    }

    /**
     * Lists all Object entities.
     *
     */
    public function waitingAction($page = 1)
    {

        $em = $this->get('doctrine.orm.entity_manager');

        $dql   = ObjectFind::getFindAllToWaiting();

        $query = $em->createQuery($dql);

       $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate($query, $page, self::ITEM_PER_PAGE);

        // parameters to template

        return $this->render('object/index.html.twig', array(
            'pagination' => $pagination,
        ));
    }


    /**
     * Lists all Object entities.
     *
     */
    public function randomAction($page = 1)
    {
        $em = $this->getDoctrine()->getManager();

        $objects = $em->getRepository('AppBundle:Object')->findAllRandom();

        $randomObjects = new ArrayCollection();

      /*  foreach($objects as $object){
          if(!$object->getIsWaiting() and $object->getActive()){
                $randomObjects->add($object);
            }
        }*/

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $objects,//$randomObjects,
            $page, /*page number*/
            2 /*limit per page*/
        );

        // parameters to template

        return $this->render('object/index.html.twig', array(
            'pagination' => $pagination,
        ));
    }
    /**
     * Creates a new Object entity.
     *
     */
    public function newAction(Request $request)
    {
        $object = new Object();
        $form = $this->createForm('AppBundle\Form\ObjectType', $object);
        $form->handleRequest($request);

        $user = $this->get('security.context')->getToken()->getUser();

        if($user == 'anon.'){

            return $this->redirect('/login/facebook');
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();


            $object->setUser($user);

            $data = $request->request->get('object');

            $object->getTags()->clear();

            $em->persist($object);
            $em->flush();

            if (!empty($data['tags'])) {

                foreach ($data['tags'] as $dataTag) {

                    $tagName = $dataTag['name'];
                    $tag = $em->getRepository('AppBundle:Tag')->findOneByName($tagName);

                    if (!$tag) {

                        $tag = new Tag();
                        $tag->setName($tagName);
                        $tag->getObjects()->add($object);
                        $em->persist($tag);
                        $em->flush();
                    }

                    $object->getTags()->add($tag);
                    $em->flush();
                }
            }

            // Ajax request
            if ($request->isXmlHttpRequest()) {
                $json = json_encode(array(
                    'id' => $object->getId(),
                ));
                $response = new Response($json);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }

            $this->getCommentsThread($request, $object);
            return $this->redirectToRoute('object_show', array('id' => $object->getId()));
        }

        return $this->render('object/new.html.twig', array(
            'object' => $object,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Object entity.
     *
     */
    public function showAction(Request $request, Object $object)
    {
        $commentsThread = $this->getCommentsThread($request, $object);
        return $this->render('object/show.html.twig', array(
            'object' => $object,
            'commentsThread' => $commentsThread
        ));
    }


    /**
     * Create new vote object;
     */
    public function newObjectVoteAction(Request $request, Object $object){
            $form = $this->createVoteForm($object);
            $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($object);
            $em->flush();

            return $this->redirectToRoute('object_edit', array('id' => $object->getId()));
        }
    }

    /**
     * Creates a form to vote a Object entity.
     *
     * @param Object $object The Object entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createVoteForm(Object $object)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('object_vote', array('id' => $object->getId())))
            ->setMethod('POST')
            ->getForm();
    }



    /**
     * Displays a form to edit an existing Object entity.
     *
     */
    public function editAction(Request $request, Object $object)
    {
        $deleteForm = $this->createDeleteForm($object);
        $editForm = $this->createForm('AppBundle\Form\ObjectType', $object);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($object);
            $em->flush();

            return $this->redirectToRoute('object_edit', array('id' => $object->getId()));
        }

        return $this->render('object/edit.html.twig', array(
            'object' => $object,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Object entity.
     *
     */
    public function deleteAction(Request $request, Object $object)
    {
        $form = $this->createDeleteForm($object);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($object);
            $em->flush();
        }

        return $this->redirectToRoute('object_index');
    }

    /**
     * Creates a form to delete a Object entity.
     *
     * @param Object $object The Object entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Object $object)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('object_delete', array('id' => $object->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }



    private function getCommentsThread(Request $request, $entity)
    {

        $class = new \ReflectionClass(get_class($entity));
        $className = strtolower($class->getShortName());

        $em = $this->getDoctrine()->getManager();
        $thread = $this->getDoctrine()
            ->getRepository('AppBundle:CommentThread')->findOneBy(
                array('type' => $className, 'typeId' => $entity->getId())
            );

        if (null === $thread) {
            $thread = $this->container->get('fos_comment.manager.thread')->createThread();
            $thread->setType($className);
            $thread->setTypeId($entity->getId());
            $thread->setPermalink($request->getUri());

            $entity->setCommentThread($thread);
            $em->persist($entity);
            $em->flush();
            // Add the thread
            $this->container->get('fos_comment.manager.thread')->saveThread($thread);
        }

        $comments = $this->container->get('fos_comment.manager.comment')->findCommentTreeByThread($thread);

        return array('comments' => $comments,
            'thread' => $thread);

    }
}
