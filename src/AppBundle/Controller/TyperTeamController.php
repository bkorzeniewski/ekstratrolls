<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\TyperTeam;
use AppBundle\Form\TyperTeamType;

/**
 * TyperTeam controller.
 *
 */
class TyperTeamController extends Controller
{
    /**
     * Lists all TyperTeam entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $typerTeams = $em->getRepository('AppBundle:TyperTeam')->findAll();

        return $this->render('typerteam/index.html.twig', array(
            'typerTeams' => $typerTeams,
        ));
    }

    /**
     * Creates a new TyperTeam entity.
     *
     */
    public function newAction(Request $request)
    {
        $typerTeam = new TyperTeam();
        $form = $this->createForm('AppBundle\Form\TyperTeamType', $typerTeam);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typerTeam);
            $em->flush();

            return $this->redirectToRoute('typerteam_show', array('id' => $typerTeam->getId()));
        }

        return $this->render('typerteam/new.html.twig', array(
            'typerTeam' => $typerTeam,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TyperTeam entity.
     *
     */
    public function showAction(TyperTeam $typerTeam)
    {
        $deleteForm = $this->createDeleteForm($typerTeam);

        return $this->render('typerteam/show.html.twig', array(
            'typerTeam' => $typerTeam,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TyperTeam entity.
     *
     */
    public function editAction(Request $request, TyperTeam $typerTeam)
    {
        $deleteForm = $this->createDeleteForm($typerTeam);
        $editForm = $this->createForm('AppBundle\Form\TyperTeamType', $typerTeam);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typerTeam);
            $em->flush();

            return $this->redirectToRoute('typerteam_edit', array('id' => $typerTeam->getId()));
        }

        return $this->render('typerteam/edit.html.twig', array(
            'typerTeam' => $typerTeam,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a TyperTeam entity.
     *
     */
    public function deleteAction(Request $request, TyperTeam $typerTeam)
    {
        $form = $this->createDeleteForm($typerTeam);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typerTeam);
            $em->flush();
        }

        return $this->redirectToRoute('typerteam_index');
    }

    /**
     * Creates a form to delete a TyperTeam entity.
     *
     * @param TyperTeam $typerTeam The TyperTeam entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TyperTeam $typerTeam)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('typerteam_delete', array('id' => $typerTeam->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
