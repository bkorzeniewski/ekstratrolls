<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Vote;

/**
 * Vote controller.
 *
 */
class VoteController extends Controller
{
    /**
     * Set vote ajax
     *
     * @param Request $request
     *
     * @return json
     *
     */
    public function setVoteAjaxAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {

            $responseData = array();

            $em = $this->getDoctrine()->getManager();

            $voteValue = $request->request->get('vote');

            try {

                $value = (int)$voteValue['value'];

                if (!($value != 1 || $value != -1)) {

                    $responseData[] = array(
                        'status' => "error",
                        'info' => 'value',
                    );

                }

            } catch (\Exception $e) {

                $responseData[] = array(
                    'status' => "error",
                    'info' => 'value',
                );

            }

            try {

                $objectId = (int)$voteValue['object'];

            } catch (\Exception $e) {
                $responseData[] = array(
                    'status' => "error",
                    'info' => 'objectId',
                );
            }


            $user = $this->get('security.context')->getToken()->getUser();

            if ($user == 'anon.') {
                $user = null;
                $responseData[] = array(
                    'status' => "error",
                    'info' => 'user_not_found',
                );
            }


            $object = $em->getRepository('AppBundle:Object')->find($objectId);

            if (!$object) {
                $responseData[] = array(
                    'status' => "error",
                    'info' => 'object_not_found',
                );
            }

            if ($user && $object && ($value == 1 || $value == -1)) {

                $vote = $em->getRepository('AppBundle:Vote')->findOneBy(
                    array('object' => $object, 'user' => $user)
                );


                if (!$vote) {
                    $vote = new Vote();
                    $vote->setUser($user)->setObject($object)->setVote($value);
                    $em->persist($vote);
                    $em->persist($object->setNumberVote($object->getNumberVote()+1));
                    $em->flush();

                    $responseData[] = array(
                        'status' => "success",
                        'info' => 'add_vote',
                        'objectId' => $vote->getObject()->getId(),
                        'vote' => $vote->getVote(),
                        );

                } elseif ($value != $vote->getVote()) {
                    $vote->setUser($user)->setObject($object)->setVote($value);
                    $em->persist($vote);
                   $em->persist($object->setNumberVote($object->getNumberVote()-1));
                    $em->flush();

                    $responseData[] = array(
                        'status' => "success",
                        'info' => 'add_vote',
                        'objectId' => $vote->getObject()->getId(),
                        'vote' => $vote->getVote(),
                        );

                } elseif ($value == $vote->getVote()) {

                    $em->remove($vote);
                    $em->persist($object->setNumberVote($object->getNumberVote() - $vote->getVote()));
                    $em->flush();

                    $responseData[] = array(
                        'status' => "success",
                        'info' => 'remove_vote',
                        'objectId' => $object->getId(),
                        'vote' => null,
                        );
                }

            }


            $json = json_encode(array(
                'data' => $responseData,
            ));


            $response = new Response($json);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }

        return null;

    }
}
