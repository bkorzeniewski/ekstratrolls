<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Questionnaire;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Answer;

use Doctrine\Common\Collections\ArrayCollection;

class QuestionnaireController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $questionnaires = $em->getRepository('AppBundle:Questionnaire')->findAll();

        return $this->render('questionnaire/index.html.twig', array(
            'questionnaires' => $questionnaires,
        ));
    }

    public function voteAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();

        $answerId = $request->request->get('answer', null);
        $referer = $request->request->get('referer', null);

        if ($answerId) {
            $answer = $em->getRepository('AppBundle:Answer')->find($answerId);
            $questionnaire = $answer->getQuestionnaire();

            foreach ($questionnaire->getAnswers() as $ans) {
                if (false === $ans->getUsers()->contains($ans)) {
                    $ans->removeUser($user);
                    $em->persist($ans);
                }
            }

            $answer->addUser($user);

            $em->persist($answer);
            $em->flush();

        }

        return $referer ? $this->redirect($referer) : $this->redirectToRoute('object_index');


    }


}
