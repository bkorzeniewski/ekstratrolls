<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TagController extends Controller
{
    /**
     * Get tags ajax
     *
     * @param Request $request
     *
     * @return json
     *
     */
    public function getTagAjaxAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {

            $query = $request->query->get('query');
            $json = null;
            $tags = array();

            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository('AppBundle:Tag')->searchInName($query);


            foreach ($entities as $tag) {
                $tags[] = $tag->getName();
            }


            $json = json_encode(array(
                "query" => "Unit",
                "suggestions" => $tags
            ));


            $response = new Response($json);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }

        return null;

    }
}
