<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\TyperMatch;
use AppBundle\Form\TyperMatchType;

/**
 * TyperMatch controller.
 *
 */
class TyperMatchController extends Controller
{
    /**
     * Lists all TyperMatch entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $typerMatches = $em->getRepository('AppBundle:TyperMatch')->findAll();

        return $this->render('typermatch/index.html.twig', array(
            'typerMatches' => $typerMatches,
        ));
    }

    /**
     * Creates a new TyperMatch entity.
     *
     */
    public function newAction(Request $request)
    {
        $typerMatch = new TyperMatch();
        $form = $this->createForm('AppBundle\Form\TyperMatchType', $typerMatch);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typerMatch);
            $em->flush();

            return $this->redirectToRoute('typermatch_show', array('id' => $typerMatch->getId()));
        }

        return $this->render('typermatch/new.html.twig', array(
            'typerMatch' => $typerMatch,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TyperMatch entity.
     *
     */
    public function showAction(TyperMatch $typerMatch)
    {
        $deleteForm = $this->createDeleteForm($typerMatch);

        return $this->render('typermatch/show.html.twig', array(
            'typerMatch' => $typerMatch,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TyperMatch entity.
     *
     */
    public function editAction(Request $request, TyperMatch $typerMatch)
    {
        $deleteForm = $this->createDeleteForm($typerMatch);
        $editForm = $this->createForm('AppBundle\Form\TyperMatchType', $typerMatch);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typerMatch);
            $em->flush();

            return $this->redirectToRoute('typermatch_edit', array('id' => $typerMatch->getId()));
        }

        return $this->render('typermatch/edit.html.twig', array(
            'typerMatch' => $typerMatch,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a TyperMatch entity.
     *
     */
    public function deleteAction(Request $request, TyperMatch $typerMatch)
    {
        $form = $this->createDeleteForm($typerMatch);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typerMatch);
            $em->flush();
        }

        return $this->redirectToRoute('typermatch_index');
    }

    /**
     * Creates a form to delete a TyperMatch entity.
     *
     * @param TyperMatch $typerMatch The TyperMatch entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TyperMatch $typerMatch)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('typermatch_delete', array('id' => $typerMatch->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
