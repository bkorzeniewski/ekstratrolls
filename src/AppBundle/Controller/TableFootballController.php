<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\TableFootball;
use AppBundle\Entity\TableFootballClub;
use AppBundle\Entity\League;
use AdminBundle\Form\TableFootballType;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * TableFootball controller.
 *
 */
class TableFootballController extends Controller
{
    /**
     * Lists all TableFootball entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tableFootball = $em->getRepository('AppBundle:TableFootball')->findMain();

        $tableFootballLeagues = $em->getRepository('AppBundle:TableFootball')->findTheLeague();


        return $this->render('tablefootball/index.html.twig', array(
            'tableFootball' => $tableFootball,
            'tableFootballLeagues' => $tableFootballLeagues
        ));
    }


    /**
     * Finds and displays a TableFootball entity.
     *
     */
    public function showAction(League $league, $leagueName)
    {
        $em = $this->getDoctrine()->getManager();

        $tableFootball = $em->getRepository('AppBundle:TableFootball')->findOneByLeague($league);

        $tableFootballLeagues = $em->getRepository('AppBundle:TableFootball')->findTheLeague();

        return $this->render('tablefootball/index.html.twig', array(
            'tableFootball' => $tableFootball,
            'tableFootballLeagues' => $tableFootballLeagues
        ));
    }



}
