<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Article;

/**
 * Article controller.
 *
 */
class ArticleController extends Controller
{
    /**
     * Lists all Article entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository('AppBundle:Article')->findAll();


        return $this->render('article/index.html.twig', array(
            'articles' => $articles,
        ));
    }


    /**
     * Finds and displays a Article entity.
     *
     */
    public function showAction(Request $request, Article $article)
    {

        $commentsThread = $this->getCommentsThread($request, $article);

        return $this->render('article/show.html.twig', array(
            'article' => $article,
            'commentsThread' => $commentsThread

        ));
    }


    private function getCommentsThread(Request $request, $entity)
    {

        $class = new \ReflectionClass(get_class($entity));
        $className = strtolower($class->getShortName());

        $em = $this->getDoctrine()->getManager();
        $thread = $this->getDoctrine()
            ->getRepository('AppBundle:CommentThread')->findOneBy(
                array('type' => $className, 'typeId' => $entity->getId())
            );

        if (null === $thread) {
            $thread = $this->container->get('fos_comment.manager.thread')->createThread();
            $thread->setType($className);
            $thread->setTypeId($entity->getId());
            $thread->setPermalink($request->getUri());

            $entity->setCommentThread($thread);
            $em->persist($entity);
            $em->flush();

            $this->container->get('fos_comment.manager.thread')->saveThread($thread);
        }

        $comments = $this->container->get('fos_comment.manager.comment')->findCommentTreeByThread($thread);

        return array('comments' => $comments,
            'thread' => $thread
        );

        }
}
