<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    public function profileAction(Request $request)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        return $this->render('user/profile.html.twig', array(
            'user' => $user,
        ));
    }
}
