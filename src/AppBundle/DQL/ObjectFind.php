<?php

namespace AppBundle\DQL;

class ObjectFind{

    /**
     * @return mixed
     */
    public static function getFindAllToMain()
    {
        return "SELECT o FROM AppBundle:Object o WHERE o.isWaiting = FALSE AND o.active = TRUE  ORDER BY o.createdAt DESC";
    }

    /**
     * @return mixed
     */
    public static function getFindAllToWaiting()
    {
        return "SELECT o FROM AppBundle:Object o WHERE o.isWaiting = TRUE AND o.active = TRUE  ORDER BY o.createdAt DESC";
    }


}