<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TableFootballClubType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('club', EntityType::class, array(
                'class' => 'AppBundle:Club',
            ))
            ->add('matches')
            ->add('won')
            ->add('lost')
            ->add('goalsScored')
            ->add('goalsAgainst')

        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TableFootballClub'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'table_football_club';
    }
}
