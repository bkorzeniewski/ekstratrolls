<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TableFootballType extends AbstractType
{

    private $container;
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->setContainer($options['container']);

        $builder
            ->add('league')
           ->add('season')
            ->add('queue')
            ->add('tableFootballClubs', CollectionType::class, array(
                'entry_type' => TableFootballClubType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
            ));

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TableFootball'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'table_football';
    }



    /**
     * @return mixed $container
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param mixed $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    public function getParent()
    {
        return 'container_aware';
    }

}
