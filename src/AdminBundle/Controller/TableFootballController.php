<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\TableFootball;
use AppBundle\Entity\TableFootballClub;
use AdminBundle\Form\TableFootballType;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * TableFootball controller.
 *
 */
class TableFootballController extends Controller
{
    /**
     * Lists all TableFootball entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tableFootballs = $em->getRepository('AppBundle:TableFootball')->findAll();

        return $this->render('AdminBundle:TableFootball:index.html.twig', array(
            'tableFootballs' => $tableFootballs,
        ));
    }

    /**
     * Creates a new TableFootball entity.
     *
     */
    public function newAction(Request $request)
    {

        $tableFootball = new TableFootball();

        $tableFootballClub = new TableFootballClub();
        $tableFootball->addTableFootballClub($tableFootballClub);
        $form = $this->createForm('AdminBundle\Form\TableFootballType', $tableFootball);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tableFootball);
            $em->flush();

            return $this->redirectToRoute('admin_tablefootball_edit', array('id' => $tableFootball->getId()));
        }

        return $this->render('AdminBundle:TableFootball:new.html.twig', array(
            'tableFootball' => $tableFootball,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TableFootball entity.
     *
     */
    public function showAction(TableFootball $tableFootball)
    {
        $deleteForm = $this->createDeleteForm($tableFootball);

        return $this->render('AdminBundle:TableFootball:show.html.twig', array(
            'tableFootball' => $tableFootball,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TableFootball entity.
     *
     */
    public function editAction(Request $request, TableFootball $tableFootball)
    {
        $deleteForm = $this->createDeleteForm($tableFootball);
        $editForm = $this->createForm('AdminBundle\Form\TableFootballType', $tableFootball);

        $originalTableFootball = new ArrayCollection();
        foreach($tableFootball->getTableFootballClubs() as $tableFootballClub){
            $originalTableFootball->add($tableFootballClub);
        }

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            foreach($originalTableFootball as $tableFootballClub){
                if (false === $tableFootball->getTableFootballClubs()->contains($tableFootballClub)) {
                    $em->remove($tableFootballClub);
                }
            }

            $em->persist($tableFootball);
            $em->flush();

            return $this->redirectToRoute('admin_tablefootball_edit', array('id' => $tableFootball->getId()));
        }

        return $this->render('AdminBundle:TableFootball:edit.html.twig', array(
            'tableFootball' => $tableFootball,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a TableFootball entity.
     *
     */
    public function deleteAction(Request $request, TableFootball $tableFootball)
    {
        $form = $this->createDeleteForm($tableFootball);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tableFootball);
            $em->flush();
        }

        return $this->redirectToRoute('admin_tablefootball_index');
    }

    /**
     * Creates a form to delete a TableFootball entity.
     *
     * @param TableFootball $tableFootball The TableFootball entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TableFootball $tableFootball)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_tablefootball_delete', array('id' => $tableFootball->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
