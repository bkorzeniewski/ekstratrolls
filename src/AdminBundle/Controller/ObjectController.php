<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Object;
use AppBundle\Entity\Tag;



/**
 * Object controller.
 *
 */
class ObjectController extends Controller
{
    /**
     * Lists all Object entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $objects = $em->getRepository('AppBundle:Object')->findAll();


        // parameters to template

        return $this->render('AdminBundle:Object:index.html.twig', array(
            'entities' => $objects,
        ));
    }

    /**
     * Creates a new Object entity.
     *
     */
    public function newAction(Request $request)
    {
        $object = new Object();
        $form = $this->createForm('AppBundle\Form\ObjectType', $object);
        $form->handleRequest($request);

        $user = $this->get('security.context')->getToken()->getUser();

        if($user == 'anon.'){

            return $this->redirect('/login/facebook');
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();


            $object->setUser($user);

            $data = $request->request->get('object');

            $object->getTags()->clear();

            $em->persist($object);
            $em->flush();

            if (!empty($data['tags'])) {

                foreach ($data['tags'] as $dataTag) {

                    $tagName = $dataTag['name'];
                    $tag = $em->getRepository('AppBundle:Tag')->findOneByName($tagName);

                    if (!$tag) {

                        $tag = new Tag();
                        $tag->setName($tagName);
                        $tag->getObjects()->add($object);
                        $em->persist($tag);
                        $em->flush();
                    }

                    $object->getTags()->add($tag);
                    $em->flush();
                }
            }

            // Ajax request
            if ($request->isXmlHttpRequest()) {
                $json = json_encode(array(
                    'id' => $object->getId(),
                ));
                $response = new Response($json);
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }

            return $this->redirectToRoute('object_show', array('id' => $object->getId()));
        }

        return $this->render('object/new.html.twig', array(
            'object' => $object,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Object entity.
     *
     */
    public function showAction(Request $request, Object $object)
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('object/show.html.twig', array(
            'object' => $object,
        ));
    }

    private function formValid($form, $entity, Object $object)
    {
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->get('security.context')->getToken()->getUser();
            $entity->setUser($user)->setObject($object);

            $em->persist($entity);
            $em->flush();

            return new RedirectResponse($this->generateUrl('object_show', array(
                'type' => $object->getType(),
                'name' => $object->getDescription(),
                'id' => $object->getId()
            )));

        }

    }

    /**
     * Displays a form to edit an existing Object entity.
     *
     */
    public function editAction(Request $request, Object $object)
    {
        $deleteForm = $this->createDeleteForm($object);
        $editForm = $this->createForm('AppBundle\Form\ObjectType', $object);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($object);
            $em->flush();

            return $this->redirectToRoute('object_edit', array('id' => $object->getId()));
        }

        return $this->render('object/edit.html.twig', array(
            'object' => $object,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Object entity.
     *
     */
    public function deleteAction(Request $request, Object $object)
    {
        $form = $this->createDeleteForm($object);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($object);
            $em->flush();
        }

        return $this->redirectToRoute('object_index');
    }

    /**
     * Creates a form to delete a Object entity.
     *
     * @param Object $object The Object entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Object $object)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('object_delete', array('id' => $object->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
