<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\Questionnaire;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Answer;

use Doctrine\Common\Collections\ArrayCollection;

class QuestionnaireController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $questionnaire = $em->getRepository('AppBundle:Questionnaire')->findAll();

        return $this->render('AdminBundle:Questionnaire:index.html.twig', array(
            'entities' => $questionnaire,
        ));
    }

    /**
     * Creates a new Questionnaire entity.
     *
     */
    public function newAction(Request $request)
    {
        $questionnaire = new Questionnaire();

        $answer = new Answer();
        $questionnaire->addAnswer($answer);

        $form = $this->createForm('AdminBundle\Form\QuestionnaireType', $questionnaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($questionnaire);
            $em->flush();

            return $this->redirectToRoute('admin_questionnaire_index');
        }

        return $this->render('AdminBundle:Questionnaire:new.html.twig', array(
            'questionnaire' => $questionnaire,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Questionnaire entity.
     *
     */
    public function editAction(Request $request, Questionnaire $questionnaire)
    {
        $deleteForm = $this->createDeleteForm($questionnaire);
        $editForm = $this->createForm('AdminBundle\Form\QuestionnaireType', $questionnaire);

        $originalAnswers = new ArrayCollection();
        foreach($questionnaire->getAnswers() as $answer){
            $originalAnswers->add($answer);
        }

        $editForm->handleRequest($request);


        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

           foreach($originalAnswers as $answer){
                if (false === $questionnaire->getAnswers()->contains($answer)) {
                    $em->remove($answer);
                }
            }


            $em->persist($questionnaire);
            $em->flush();

            return $this->redirectToRoute('admin_questionnaire_edit', array('id' => $questionnaire->getId()));
        }

        return $this->render('AdminBundle:Questionnaire:edit.html.twig', array(
            'questionnaire' => $questionnaire,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Questionnaire entity.
     *
     */
    public function deleteAction(Request $request, Questionnaire $questionnaire)
    {
        $form = $this->createDeleteForm($questionnaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($questionnaire);
            $em->flush();
        }

        return $this->redirectToRoute('admin_questionnaire_index');
    }

    /**
     * Creates a form to delete a Questionnaire entity.
     *
     * @param Questionnaire $questionnaire The Questionnaire entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Questionnaire $questionnaire)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_questionnaire_delete', array('id' => $questionnaire->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

}
