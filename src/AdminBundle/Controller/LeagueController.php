<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\League;
use AdminBundle\Form\LeagueType;

/**
 * League controller.
 *
 */
class LeagueController extends Controller
{
    /**
     * Lists all League entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $leagues = $em->getRepository('AppBundle:League')->findAll();

        return $this->render('AdminBundle:League:index.html.twig', array(
            'leagues' => $leagues,
        ));
    }

    /**
     * Creates a new League entity.
     *
     */
    public function newAction(Request $request)
    {
        $league = new League();
        $form = $this->createForm('AdminBundle\Form\LeagueType', $league);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($league);
            $em->flush();

            return $this->redirectToRoute('admin_league_index', array('id' => $league->getId()));
        }

        return $this->render('AdminBundle:League:new.html.twig', array(
            'league' => $league,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a League entity.
     *
     */
    public function showAction(League $league)
    {
        $deleteForm = $this->createDeleteForm($league);

        return $this->render('AdminBundle:League:show.html.twig', array(
            'league' => $league,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing League entity.
     *
     */
    public function editAction(Request $request, League $league)
    {
        $deleteForm = $this->createDeleteForm($league);
        $editForm = $this->createForm('AdminBundle\Form\LeagueType', $league);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($league);
            $em->flush();

            return $this->redirectToRoute('admin_league_edit', array('id' => $league->getId()));
        }

        return $this->render('AdminBundle:League:edit.html.twig', array(
            'league' => $league,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a League entity.
     *
     */
    public function deleteAction(Request $request, League $league)
    {
        $form = $this->createDeleteForm($league);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($league);
            $em->flush();
        }

        return $this->redirectToRoute('admin_league_index');
    }

    /**
     * Creates a form to delete a League entity.
     *
     * @param League $league The League entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(League $league)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_league_delete', array('id' => $league->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
