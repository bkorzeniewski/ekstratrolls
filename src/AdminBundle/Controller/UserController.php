<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AppBundle:User')->findAll();

        return $this->render('AdminBundle:User:index.html.twig', array(
            'entities' => $users,
        ));
    }

    public function profileAction(Request $request)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        return $this->render('user/profile.html.twig', array(
            'user' => $user,
        ));
    }
}
