<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Footballer;
use AdminBundle\Form\FootballerType;

/**
 * Footballer controller.
 *
 */
class FootballerController extends Controller
{
    /**
     * Lists all Footballer entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $footballers = $em->getRepository('AppBundle:Footballer')->findAll();

        return $this->render('AdminBundle:Footballer:index.html.twig', array(
            'footballers' => $footballers,
        ));
    }

    /**
     * Creates a new Footballer entity.
     *
     */
    public function newAction(Request $request)
    {
        $footballer = new Footballer();
        $form = $this->createForm('AdminBundle\Form\FootballerType', $footballer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($footballer);
            $em->flush();

            return $this->redirectToRoute('admin_footballer_index');
        }

        return $this->render('AdminBundle:Footballer:new.html.twig', array(
            'footballer' => $footballer,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Footballer entity.
     *
     */
    public function showAction(Footballer $footballer)
    {
        $deleteForm = $this->createDeleteForm($footballer);

        return $this->render('AdminBundle:Footballer:show.html.twig', array(
            'footballer' => $footballer,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Footballer entity.
     *
     */
    public function editAction(Request $request, Footballer $footballer)
    {
        $deleteForm = $this->createDeleteForm($footballer);
        $editForm = $this->createForm('AdminBundle\Form\FootballerType', $footballer);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($footballer);
            $em->flush();

            return $this->redirectToRoute('admin_footballer_edit', array('id' => $footballer->getId()));
        }

        return $this->render('AdminBundle:Footballer:edit.html.twig', array(
            'footballer' => $footballer,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Footballer entity.
     *
     */
    public function deleteAction(Request $request, Footballer $footballer)
    {
        $form = $this->createDeleteForm($footballer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($footballer);
            $em->flush();
        }

        return $this->redirectToRoute('admin_footballer_index');
    }

    /**
     * Creates a form to delete a Footballer entity.
     *
     * @param Footballer $footballer The Footballer entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Footballer $footballer)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_footballer_delete', array('id' => $footballer->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
