<?php
namespace AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;


    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Ekran Startowy')->setUri('/#')->setAttribute('icon', 'fa fa-home');
        $menu['Ekran Startowy']->addChild('Dodaj')->setUri('/#');

        $menu->addChild('Użytkownicy', array('route' => 'admin_user_index'))->setAttribute('icon', 'fa fa-user');

        $menu->addChild('Memy', array('route' => 'admin_object_index'))->setAttribute('icon', 'fa fa-picture-o');

        $menu->addChild('Tabele', array('route' => 'admin_tablefootball_index'))->setAttribute('icon', 'fa fa-table');

        $menu->addChild('Artykuły', array('route' => 'admin_article_index'))->setAttribute('icon', 'fa fa-pencil-square');

        $menu->addChild('Ankiety', array('route' => 'admin_questionnaire_index'))->setAttribute('icon', 'fa fa-futbol-o');
        $menu['Ankiety']->addChild('Dodaj', array('route' => 'admin_questionnaire_new'));

        $menu->addChild('Liga typerów')->setUri('/#')->setAttribute('icon', 'fa fa-list');

        $menu->addChild('Ligi', array('route' => 'admin_league_index'))->setAttribute('icon', 'fa fa-table');
        $menu['Ligi']->addChild('Sezony', array('route' => 'admin_season_index'));

        $menu->addChild('Piłkarze', array('route' => 'admin_footballer_index'))->setAttribute('icon', 'fa fa-male');

        $menu->addChild('Kluby', array('route' => 'admin_club_index'))->setAttribute('icon', 'fa fa-users');


        return $menu;

    }

    public function topMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Kontakt')->setUri('/#');
        $menu->addChild('Regulamin')->setUri('/#');

        $menu->addChild('Logowanie')->setUri('/#');
        $menu->addChild('Rejestracja')->setUri('/#');

        return $menu;
    }

    public function profileMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Profil', array('route' => 'user_profile'));
        $menu->addChild('Ustawienia', array('route' => 'object_index'));
        $menu->addChild('Wyloguj')->setUri('/logout');;

        return $menu;
    }


}