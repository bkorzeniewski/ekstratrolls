/// <reference path="jquery.d.ts" />

class CollectionForm {
    private _collectionHolder:JQuery;
    private _buttonAdd:JQuery;

    constructor(collectionHolderName?:string) {

        this._collectionHolder = collectionHolderName ?  $(collectionHolderName): $('#collection-form');
        this._collectionHolder;
        this._collectionHolder.data('index', this._collectionHolder.find(':input').length);
        this._buttonAdd = $('<button type="button" id="add_new_form" >dodaj nowy</button>');
        this.addAllRemoveButtonForm();
        this.addNewButtonAdd();

    }

    private addNewForm():void {
        var prototype = this._collectionHolder.data('prototype').toString();

        var index = +this._collectionHolder.data('index');

        var newForm = prototype.replace(/__name__/g, index.toString());

        this._collectionHolder.data('index', index + 1);

        var newFormLi = $('<li></li>').append(newForm);

        this._collectionHolder.append(newFormLi);
        this.addRemoveButton(newFormLi);

    }

    private addNewButtonAdd():void{
        var newLiButton = $('<li></li>').append(this._buttonAdd);
        this._collectionHolder.prepend(newLiButton);
       // this.addNewForm();
        this._buttonAdd.on('click',  (event: JQueryEventObject) =>  {
            this.addNewForm();
        })
    }

    private addRemoveButton(formLi: JQuery):void{
        var removeFormButton = $('<button type="button" class="remove_form" >usuń</button>');

        if(!formLi.find(removeFormButton).length){
            formLi.append(removeFormButton);
        }

        removeFormButton.on('click', function(e) {
            formLi.remove();
        });
    }

    private addAllRemoveButtonForm():void{
        var _this = this;
        this._collectionHolder.find('li').each(function(){
         _this.addRemoveButton($(this));
        });

    }





}