/// <reference path="jquery.d.ts" />
var CollectionForm = (function () {
    function CollectionForm(collectionHolderName) {
        this._collectionHolder = collectionHolderName ? $(collectionHolderName) : $('#collection-form');
        this._collectionHolder;
        this._collectionHolder.data('index', this._collectionHolder.find(':input').length);
        this._buttonAdd = $('<button type="button" id="add_new_form" >dodaj nowy</button>');
        this.addAllRemoveButtonForm();
        this.addNewButtonAdd();
    }
    CollectionForm.prototype.addNewForm = function () {
        var prototype = this._collectionHolder.data('prototype').toString();
        var index = +this._collectionHolder.data('index');
        var newForm = prototype.replace(/__name__/g, index.toString());
        this._collectionHolder.data('index', index + 1);
        var newFormLi = $('<li></li>').append(newForm);
        this._collectionHolder.append(newFormLi);
        this.addRemoveButton(newFormLi);
    };
    CollectionForm.prototype.addNewButtonAdd = function () {
        var _this = this;
        var newLiButton = $('<li></li>').append(this._buttonAdd);
        this._collectionHolder.prepend(newLiButton);
        // this.addNewForm();
        this._buttonAdd.on('click', function (event) {
            _this.addNewForm();
        });
    };
    CollectionForm.prototype.addRemoveButton = function (formLi) {
        var removeFormButton = $('<button type="button" class="remove_form" >usuń</button>');
        if (!formLi.find(removeFormButton).length) {
            formLi.append(removeFormButton);
        }
        removeFormButton.on('click', function (e) {
            formLi.remove();
        });
    };
    CollectionForm.prototype.addAllRemoveButtonForm = function () {
        var _this = this;
        this._collectionHolder.find('li').each(function () {
            _this.addRemoveButton($(this));
        });
    };
    return CollectionForm;
})();
//# sourceMappingURL=collectionForm.js.map