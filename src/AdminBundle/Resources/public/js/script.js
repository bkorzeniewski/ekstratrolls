var Answer = function () {
    var $addButton = $('#add_answer');
    var $collectionHolder = $('#answers');

    $collectionHolder.data('index', $collectionHolder.find(':input').length);
    $addButton.on('click', function () {
        addAnswerForm();
    });

    var addAnswerForm = function () {
        var $tr = $('tr.answer');
        // Get the data-prototype explained earlier
        var prototype = $collectionHolder.data('prototype');

        // get the new index
        var index = $collectionHolder.data('index');

        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var newForm = prototype.replace(/__name__/g, index);
        var input = $(newForm).find(':input').clone();

        // increase the index with one for the next item
        $collectionHolder.data('index', index + 1);

        var tr = '<tr class="answer"><th><span>' + (index ) + ')</span> Odpowiedź:</th> <td>' + $('<div>').append(input.addClass('form-control input-sm')).html()
            + ' </td><td><button type="button" class="remove_answer btn btn-danger btn-sm"><i class="fa fa-trash-o"></i> usuń</button></td></tr>';
        // Display the form in the page in an li, before the "Add a tag" link li
        $tr.last().after(tr);
        removeAnswerForm();
    };

    var removeAnswerForm = function () {
        var $removeButton = $('.remove_answer');

        $removeButton.one('click', function () {
            $(this).closest('.answer').remove();
            disableRemoveButton();
        });
        disableRemoveButton();
    };

    var disableRemoveButton = function () {
            var count = $('.answer').length;
            if (count <= 1) {
                $('.remove_answer').css('visibility', 'hidden')
            } else {
                $('.remove_answer').css('visibility', 'visible');
            }
        updateNumber();
        };

    var updateNumber = function(){
        $('.answer').each(function(index){
            $(this).find('span').text((index+1)+')')
        })
    };


    removeAnswerForm();
};
var TableFootball = function () {
    var $addButton = $('#add_table_football_club');
    var $collectionHolder = $('#table_football_clubs');

    $collectionHolder.data('index', $collectionHolder.find(':input').length);
    $addButton.on('click', function () {
        addTableFootballClubForm();
    });

    var addTableFootballClubForm = function () {
        var $tr = $('tr.table_football_club');
        // Get the data-prototype explained earlier
        var prototype = $collectionHolder.data('prototype');

        // get the new index
        var index = $collectionHolder.data('index');

        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var newForm = prototype.replace(/__name__/g, index);
        var input = $(newForm).find(':input').clone();

        // increase the index with one for the next item
        $collectionHolder.data('index', index + 1);

        var tr = '<tr class="table_football_club"><th><span>' + (index ) + ')</span> Odpowiedź:</th> <td>' + $('<div>').append(input.addClass('form-control input-sm')).html()
            + ' </td><td><button type="button" class="remove_answer btn btn-danger btn-sm"><i class="fa fa-trash-o"></i> usuń</button></td></tr>';
        // Display the form in the page in an li, before the "Add a tag" link li
        $tr.last().after(tr);
        removeAnswerForm();
    };

    var removeAnswerForm = function () {
        var $removeButton = $('.remove_answer');

        $removeButton.one('click', function () {
            $(this).closest('.answer').remove();
            disableRemoveButton();
        });
        disableRemoveButton();
    };

    var disableRemoveButton = function () {
        var count = $('.answer').length;
        if (count <= 1) {
            $('.remove_answer').css('visibility', 'hidden')
        } else {
            $('.remove_answer').css('visibility', 'visible');
        }
        updateNumber();
    };

    var updateNumber = function(){
        $('.answer').each(function(index){
            $(this).find('span').text((index+1)+')')
        })
    };


    removeAnswerForm();
};

$(document).ready(function () {
    Answer();
});
